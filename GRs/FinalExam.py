#!/usr/bin/env python3
"""
Final Exam, CS210, Fall 2017
"""

# Fill in the metadata
__author__ = "C3C Allison Wong"  # Your name. Ex: John Doe
__section__ = "M3"  # Your section. Ex: M1
__version__ = "B"  # Test version. Ex: A
__date__ = "13 Dec 2017"  # Today's date. Ex: 25 Dec 2017


# ======================================================================
# Main
# ======================================================================
def main():
    """ Comment and uncomment these lines as needed to work on each problem. """
    problem1()
    # problem2()
    # problem3()
    # problem4()
    # problem5()
    # problem6()
    # problem7()
    # problem8()


# ======================================================================
# Problem 1                                                ____ / 30 pts
# ======================================================================
def problem1():
    print_problem_name()

    # TODO 1b
    if nautical_miles_to_feet(1) != 0.0:
        print("Incorrect: 1, 0.0")
    elif nautical_miles_to_feet(200) != 1215224.0:
        print("Incorrect: 200, 1215224.0")
    elif nautical_miles_to_feet(6000) != 0.0:
        print("Incorrect: 6000, 0.0")


# TODO 1a
def nautical_miles_to_feet(nautical_mile):
    feet = nautical_mile * 6076.12
    statue_mile = feet / 5280
    if 110 < statue_mile < 410:
        return feet
    else:
        return 0.0


# ======================================================================
# Problem 2                                                ____ / 30 pts
# ======================================================================
def problem2():
    print_problem_name()

    # TODO 2
    filename = "../Data/alice_in_wonderland.txt"
    write_filename = "alicewords.txt"
    with open(filename, "r") as f:
        lines = f.read().splitlines()
    with open(write_filename, "w") as w:
        for line in lines:
            if len(line) == 0:
                print(" ", end="", file=w)
            else:
                words = line.split()
                shortest_length = 100000
                for word in words:
                    if len(word) < shortest_length:
                        shortest_length = len(word)
                        shortest_word = word
                print(shortest_word, end=" ", file=w)


# ======================================================================
# Problem 3                                                ____ / 30 pts
# ======================================================================
def problem3():
    print_problem_name()

    # TODO 3b
    if find_max([3, 67, 45, 23, 18, 4, 89], 3, 5) != 23:
        print("Incorrect: 23")
    elif find_max([42, 78, 14, 13, 85, 27], 2, 7) != 85:
        print("incorrect: 85")
    elif find_max([22, 58, 12, 10], -1, 2) != 58:
        print("Incorrect: 58")


# TODO 3a
def find_max(integers, first, last):
    """
    Finds the maximum integer in a list within given index bounds
    :param list integers: a given list of integers
    :param int first: the first index at which to start finding the maximum
    :param int last: the last index at which to stop finding the maximum, inclusive
    :return: maximum: the maximum integer in the given list within the given index bounds
    :rtype: int
    """
    if first < 0:
        first = 0
    if last > len(integers) - 1:
        last = len(integers) - 1
    maximum = 0
    for i in range(first, last + 1, 1):
        if integers[i] > maximum:
            maximum = integers[i]
    return maximum


# ======================================================================
# Problem 4                                                ____ / 30 pts
# ======================================================================
def problem4():
    print_problem_name()

    # TODO 4c
    board1 = create_checkers(6)
    print_checkers(board1)
    board2 = create_checkers(9)
    print_checkers(board2)


# TODO 4a
def create_checkers(size):
    board = list()
    empty_rows = size - 6  # Number of empty, piece-less rows

    # All "B" piece rows
    for _ in range(3):
        if (_ + 2) % 2 == 0:  # If on an even row, among the three
            row = list()
            for i in range(size):
                if (i + 2) % 2 == 0:
                    row.append("_")
                elif (i + 2) % 2 == 1:
                    row.append("B")
            board.append(row)
        elif (_ + 2) % 2 == 1:  # If on an odd row, among the three
            row = list()
            for i in range(size):
                if (i + 2) % 2 == 1:
                    row.append("_")
                elif (i + 2) % 2 == 0:
                    row.append("B")
            board.append(row)

    # All empty, piece-less rows
    for _ in range(empty_rows):
        row = list()
        for __ in range(size):
            row.append("_")
        board.append(row)

    # All "R" piece rows
    for _ in range(3):
        if (_ + 2) % 2 == 1:  # If on an odd row, among the three
            row = list()
            for i in range(size):
                if (i + 2) % 2 == 0:
                    row.append("_")
                elif (i + 2) % 2 == 1:
                    row.append("R")
            board.append(row)
        elif (_ + 2) % 2 == 0:  # If on an even row, among the three
            row = list()
            for i in range(size):
                if (i + 2) % 2 == 1:
                    row.append("_")
                elif (i + 2) % 2 == 0:
                    row.append("R")
            board.append(row)
    # I wasn't sure why in the example for the size 9 board, the first Red line of the board began with a space.
    # As it is no longer alternating with the black pieces, like it is in the size 6 example and in actual checkers.
    # So I chose to begin the R side with a Red piece, rather than a space, and alternate from there.

    return board


# TODO 4b
def print_checkers(board):
    for row in board:
        for each in row:
            print(each, end=" ")
        print("\n", end="")
    print("\n", end="")


# ======================================================================
# Problem 5                                                ____ / 30 pts
# ======================================================================
def problem5():
    print_problem_name()

    # TODO 5b
    # Create 4 instances of Box
    box1 = Box(10, 10, 10)
    box2 = Box(5, 7, 8)
    box3 = Box(9, 15, 2)
    box4 = Box(5, 7, 8)

    # Test __str__ method, and if resetting a write-able attribute will update the volume as well
    print(box1)
    box1.depth = 3
    print(box1)

    # Test if the equals() function works for both same and different boxes
    if box2.equals(box4) is not True:
        print("Incorrect: True")
    elif box2.equals(box3) is not False:
        print("Incorrect: False")


# TODO 5a
class Box:
    def __init__(self, w, h, d):
        self.__width = w
        self.__height = h
        self.__depth = d
        self.__volume = self.__width * self.__height * self.__depth

    def __str__(self):
        return "Width: {}, Height: {}, Depth: {}, Volume: {}.".format(self.__width, self.__height, self.__depth,
                                                                      self.__volume)

    @property
    def width(self):
        return self.__width

    @width.setter
    def width(self, new_width):
        self.__width = new_width
        self.update_volume()

    @property
    def height(self):
        return self.__height

    @height.setter
    def height(self, new_height):
        self.__height = new_height
        self.update_volume()

    @property
    def depth(self):
        return self.__depth

    @depth.setter
    def depth(self, new_depth):
        self.__depth = new_depth
        self.update_volume()

    @property
    def volume(self):
        return self.__volume

    def equals(self, other_box):
        if self.__width == other_box.width and self.__height == other_box.height and self.__depth == other_box.depth:
            return True
        else:
            return False

    def update_volume(self):
        self.__volume = self.__width * self.__height * self.__depth


# ======================================================================
# Problem 6                                                ____ / 30 pts
# ======================================================================
def problem6():
    print_problem_name()

    # TODO 6b
    if remove_duplicates("ATCG") != "ATCG":
        print("Incorrect")
    elif remove_duplicates("AAATCG") != "ATCG":
        print("Incorrect")
    elif remove_duplicates("AATTTTCCGG") != "ATCG":
        print("Incorrect")
    elif remove_duplicates("ATCGATTGAGCTCTAGG") != "ATCGATGAGCTCTAG":
        print("Incorrect")


# TODO 6a
def remove_duplicates(sequence):
    if len(sequence) <= 1:
        return sequence
    else:
        if sequence[1] == sequence[0]:
            return "" + remove_duplicates(sequence[1:])
        else:
            new_sequence = sequence[0] + remove_duplicates(sequence[1:])
            return new_sequence


# ======================================================================
# Problem 7                                                ____ / 30 pts
# ======================================================================
def problem7():
    print_problem_name()

    # TODO 7
    print_results(1, 100)
    print_results(7, 50)
    print_results(100, 200)
    print_results(300, 500)  # Takes a while to find 496..


# Prints the output
def print_results(start, end):
    print("Start: {}".format(start))
    print("End: {}".format(end))
    print("Perfect Numbers:")
    if len(check_numbers(start, end)) == 0:
        print("None")
    for number in check_numbers(start, end):
        print(number)
    print("\n", end="")


# Returns a list of perfect numbers in the given range
def check_numbers(start, end):
    perfect_numbers = list()
    for i in range(start, end + 1, 1):
        if is_perfect(i) is True:
            perfect_numbers.append(i)
    return perfect_numbers


# Checks if the number is a perfect number
def is_perfect(num):
    if find_sum(find_factors(num)) == num:
        return True
    else:
        return False


# Finds the sum of all the numbers in the list of factors
def find_sum(factors):
    if len(factors) <= 1:
        return factors[0]
    else:
        return factors[0] + find_sum(factors[1:])


# Returns a list of all the factors of the number
def find_factors(num):
    factors = [1]
    if num == 1:
        factors = [0]
    # Checks all numbers if they have a match, making them factors of the given number
    for i in range(1, num):
        if i in factors:
            pass
        else:
            for j in range(1, num):
                if i * j == num:
                    factors.append(i)
                    factors.append(j)
    return factors


# ======================================================================
# Problem 8                                                ____ / 40 pts
# ======================================================================
def problem8():
    print_problem_name()

    # TODO 8b
    filename = "../Data/Names.txt"
    print(load_families(filename))

    # TODO 8c
    for family in load_families(filename):
        print(family)
        for person in load_families(filename)[family]:
            print("    ", end="")
            print(person["first_name"][0], end="")
            print(".", person["middle_name"])
            # Instructions did not clarify to sort or not


# TODO 8a
def load_families(filename):
    families = {}  # type: dict[str, list[dict]]
    with open(filename, "r") as f:
        names = f.read().splitlines()
    for name in names:
        last_name = name.split()[2]
        name_dict = dict()
        name_dict["first_name"] = name.split()[0]
        name_dict["middle_name"] = name.split()[1]
        name_dict["last_name"] = last_name
        if last_name in families:
            families[last_name].append(name_dict)
        else:
            families[last_name] = [name_dict]

    return families


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def print_problem_name():
    """Print the name and docstring of the calling function (i.e., the current problem.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name]) or "No docstring"
        doc_color = "\033[91m" if doc is None else "\033[92m"
        print('\n\033[94m{}\n{}\n{}{}\033[99m'.format(name, "=" * len(name), doc_color, doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


if __name__ == "__main__":
    print(__doc__.strip())
    print("Author:",
          (__author__ if __author__ else "\033[91mBLANK (You must fill in the __author__ metadata)\033[0m"))
    print("Section:",
          (__section__ if __section__ else "\033[91mBLANK (You must fill in the __section__ metadata)\033[0m"))
    print("Test Version:",
          (__version__ if __version__ else "\033[91mBLANK (You must fill in the __version__ metadata)\033[0m"))
    print("Date:", (__date__ if __date__ else "\033[91mBLANK (You must fill in the __date__ metadata)\033[0m"))
    _ = b'CmltcG9ydCBnZXRwYXNzLCBoYXNobGliLCBjb2RlY3MsIHN0cmluZyBhcyBfX1MKdSA9IGdldHBhc3MuZ2V0dXNlcigpCmggPSBoYXN' + \
        b'obGliLnNoYTI1Nih1LmVuY29kZSgpKS5oZXhkaWdlc3QoKQpyID0gY29kZWNzLmVuY29kZSh1Lmxvd2VyKCkudHJhbnNsYXRlKHtvcm' + \
        b'Qoayk6IE5vbmUgZm9yIGsgaW4gX19TLmRpZ2l0c30pLnJlcGxhY2UoJy4nLCcnKSwgJ3JvdF8xMycpCndpdGggb3BlbihfX2ZpbGVfX' + \
        b'ywgInIiKSBhcyBmOgogICAgaWYgaCBub3QgaW4gZi5yZWFkKCk6CiAgICAgICAgd2l0aCBvcGVuKF9fZmlsZV9fLCAiYSIpIGFzIGY6' + \
        b'CiAgICAgICAgICAgIHByaW50KCIjIiwgaCwgciwgZmlsZT1mKQo='
    # noinspection PyBroadException
    try:
        import base64

        eval(compile(base64.b64decode(_), '<string>', "exec"))
    except:
        pass
    finally:
        main()
# 8fe2c883fe0bce1c11b308783baa702e2c2bb0085f6210c7daa5998870dacd19 pnyyvfbajbat
