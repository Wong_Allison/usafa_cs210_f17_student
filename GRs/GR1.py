#!/usr/bin/env python3
"""
GR1, CS210, Fall 2017
"""
import math
import random
import easygui
import turtle

# Fill in the metadata
__author__ = "Allison Wong"  # Your name. Ex: John Doe
__section__ = "M3"  # Your section. Ex: M1
__version__ = "A"  # Test version. Ex: A
__date__ = "19 Sept 2017"  # Today's date. Ex: 25 Dec 2017

# Define several useful constants to be used by the Turtle graphics.
WIDTH = 960  # Usually 720, 960, 1024, 1280, 1600, or 1920.
HEIGHT = WIDTH * 9 // 16  # Produces the eye-pleasing 16:9 HD aspect ratio.
MARGIN = WIDTH // 30  # Somewhat arbitrary value, but it looks nice.
FONT_SIZE = MARGIN // 2  # Somewhat arbitrary value, but it looks nice.
DRAW_FAST = True  # Set to True for fast, non-animated turtle movement.

""" The following code segments are provided for your reference.
    You are free to copy/paste them into your answers below.

    # Use the writing turtle to write a message, centered at its location, in a bold font.
    writer.write( "Hello, World!", align="center", font=( "Courier", FONT_SIZE, "bold" ) )

    # An easygui message box to display a formatted string.
    easygui.msgbox( "pi = {:.2f}".format( 22 / 7 ), "Result" )

    # An easygui enter box for entering a string.
    s = easygui.enterbox( "Enter a string:", "Input" )

    # An easygui integer box for entering a positive integer.
    n = easygui.integerbox( "Enter a positive integer:", "Input", 42, 1, 2 ** 31 )

    # Read the entire contents of a file into a single string.
    with open( easygui.fileopenbox( default="../Data/*.txt" ) ) as data_file:
        data_string = data_file.read()

    # Read the entire contents of a file into a list of strings, one per word.
    with open( easygui.fileopenbox( default="../Data/*.txt" ) ) as data_file:
        data_words = data_file.read().split()

    # Read the entire contents of a file into a list of strings, one per line.
    with open( easygui.fileopenbox( default="../Data/*.txt" ) ) as data_file:
        data_lines = data_file.read().splitlines()
"""


# ======================================================================
# Main
# ======================================================================
def main():
    """ Comment and uncomment these lines as needed to work on each problem. """
    # problem1()
    # problem2()
    # problem3()
    # problem4()
    problem5()


# ======================================================================
# Problem 1                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# ======================================================================
def problem1():
    """Problem 1 from the GR."""
    print_problem_name()

    # TODO 1b
    print("Hands  Feet")
    print("=====  =====")
    for hands in range(10,21,1):
        print("{}     {:>4.2f}".format(hands, hands_to_feet(float(hands))))


# TODO 1a
def hands_to_feet(hands):
    """Converts hands to feet

    :param float hands: number of hands
    :return: float feet
    """
    feet = (hands*4)/12
    return feet


# ======================================================================
# Problem 2                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# ======================================================================
def problem2():
    """Problem 2 from the GR."""
    print_problem_name()

    # TODO 2b
    rolls = easygui.integerbox("Please enter how many times the dice is to be rolled.", "Rolls", None, 1, 1000)
    target = easygui.integerbox("Please enter the target value to count.", "Target", None, 2, 12)
    result = count_dice(rolls, target)
    if result == 1:
        easygui.msgbox("In {} rolls, a {} came up once.".format(rolls, target))
    elif result == 2:
        easygui.msgbox("In {} rolls, a {} came up twice.".format(rolls, target))
    else:
        easygui.msgbox("In {} rolls, a {} came up {} times.".format(rolls, target, count_dice(rolls, target)))


# TODO 2a
def count_dice(rolls, target):
    times = 0
    for i in range(1, rolls+1, 1):
        rand1 = random.randint(1, 6)
        rand2 = random.randint(1, 6)
        if (rand1 + rand2) == target:
            times += 1
    return times


# ======================================================================
# Problem 3                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# Including 5 pts for good docstring
# ======================================================================
def problem3():
    """Problem 3 from the GR."""
    print_problem_name()

    # TODO 3b
    int1 = easygui.integerbox("Please enter the first positive integer.", "One", 42)
    int2 = easygui.integerbox("Please enter the second positive integer.", "Two", 28)
    int3 = easygui.integerbox("Please enter the third positive integer.", "Three", 63)
    int4 = easygui.integerbox("Please enter the fourth positive integer.", "Four", 84)
    easygui.msgbox("The GCD of {}, {}, {}, and {} is {}.".format(int1, int2, int3, int4,
                                                                 gcd(gcd(int1, int2), gcd(int3, int4))))


# TODO 3a
def gcd(int1, int2):
    """Calculates the Greatest Common Divisor of 2 integers using Euclid's algorithm.

    :param int int1: first positive integer
    :param int int2: second positive integer
    :return: int int1: int2 subtracted from the original int1, if int1 was greater than int2
    :return: int int2: int1 subtracted from the original int2, if int 1 was not greater than int2
    """
    while int1 != int2:
        if int1 > int2:
            int1 = int1 - int2
            return int1
        else:
            int2 = int2 - int1
            return int2


# ======================================================================
# Problem 4                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# ======================================================================
def problem4():
    """Problem 4 from the GR."""
    print_problem_name()

    with open(easygui.fileopenbox(default="../Data/*.txt")) as data_file:
        data_words = data_file.read().split()
        # Reading in a file followed by split() creates a list of words
        # such as you have used in labs. For example:
        # data_words = ["It", "was", "the", "worst", "of", "times"]

    # TODO 4b
    male_pronouns = ["he", "him"]
    female_pronouns = ["she", "her"]
    male = count_word(data_words, male_pronouns)
    female = count_word(data_words, female_pronouns)
    if male > female:
        easygui.msgbox("There are more male than female pronouns.")
    elif female > male:
        easygui.msgbox("There are more female than male pronouns.")
    elif male == female:
        easygui.msgbox("There are equal female and male pronouns.")


# TODO 4a
def count_word(counted, words):
    count = 0
    for i in range(0, len(counted), 1):
        for j in range(0, 1, 1):
            if counted[i] == words[j]:
                count += 1
    return count


# ======================================================================
# Problem 5                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# ======================================================================
def problem5():
    """Problem 5 from the GR."""
    print_problem_name()

    # Create the turtle screen and two turtles
    screen, artist, writer = turtle_setup()
    artist.ht()
    writer.ht()

    # TODO 5
    # Draw 2 circles
    artist.penup()
    artist.goto(0, -(HEIGHT-(2*MARGIN))/2)
    artist.pendown()
    outer_radius = (HEIGHT-(2*MARGIN))/2
    artist.circle(outer_radius)
    artist.penup()
    artist.goto(0, (-(HEIGHT-(2*MARGIN)))/4)
    artist.pendown()
    inner_radius = (HEIGHT-(2*MARGIN))/4
    artist.circle(inner_radius)

    # Dots and score
    i = 0
    misses = 0
    score = 0
    for i in range(1,11,1):
        artist.penup()
        x = random.randint(-(WIDTH/2), WIDTH/2)
        y = random.randint(-(HEIGHT/2), HEIGHT/2)
        artist.goto(x, y)
        distance = math.sqrt((x**2)+(y**2))
        # figured it out with one minute to go
        if distance > outer_radius:
            color = "red"
            misses += 1
        elif distance > inner_radius and distance < outer_radius:
            color = "yellow"
            score += 1
        elif distance < inner_radius:
            color = "green"
            score += 2
        # if (x > -(outer_radius) and x < outer_radius) and (y > -(outer_radius) and y < outer_radius):
        #     if (x > -(inner_radius) and x < inner_radius) and (y > -(inner_radius) and y < inner_radius):
        #         color = "green"
        #         score += 2
        #     else:
        #         color = "yellow"
        #         score += 1

        artist.dot(MARGIN, color)
        if misses > 7:
            break

    # Prints
    if misses >= 7:
        writer.penup()
        writer.goto((-(inner_radius)+MARGIN), 0)
        writer.pendown()
        writer.write("Too many misses.")
        writer.penup()
        writer.goto((-(inner_radius)+MARGIN), -(MARGIN/2))
        writer.pendown()
        writer.write("Try again.")
        writer.penup()
    else:
        writer.penup()
        writer.goto((-(inner_radius)+MARGIN), 0)
        writer.pendown()
        writer.write("Shots Taken: {}".format(i))
        writer.penup()
        writer.goto((-(inner_radius)+MARGIN), -(MARGIN/2))
        writer.pendown()
        writer.write("Final Score: {}".format(score))

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def turtle_setup():
    """Setup the turtle environment with a screen and two turtles, one for drawing and one for writing.

    Using separate turtles for drawing and writing makes it easy to clear one or the other by
    doing artist.clear() or writer.clear() to clear only the drawing or writing, respectively.

    :return: The screen, a drawing turtle, and a writing turtle.
    :rtype: (turtle.Screen, turtle.Turtle, turtle.Turtle)
    """
    #  ___   ___     _  _  ___ _____    __  __  ___  ___ ___ _____   __
    # |   \ / _ \   | \| |/ _ \_   _|  |  \/  |/ _ \|   \_ _| __\ \ / /
    # | |) | (_) |  | .` | (_) || |    | |\/| | (_) | |) | || _| \ V /
    # |___/ \___/   |_|\_|\___/ |_|    |_|  |_|\___/|___/___|_|   |_|
    #  _____ _  _ ___ ___    ___ _   _ _  _  ___ _____ ___ ___  _  _
    # |_   _| || |_ _/ __|  | __| | | | \| |/ __|_   _|_ _/ _ \| \| |
    #   | | | __ || |\__ \  | _|| |_| | .` | (__  | |  | | (_) | .` |
    #   |_| |_||_|___|___/  |_|  \___/|_|\_|\___| |_| |___\___/|_|\_|
    #
    # Create the turtle graphics screen and set a few basic properties.
    screen = turtle.Screen()
    screen.setup(WIDTH, HEIGHT, MARGIN, MARGIN)
    screen.bgcolor("SkyBlue")

    # Create two turtles, one for drawing and one for writing.
    turtle.TurtleScreen._RUNNING = True  # Get around bug in v3.5.2 http://bugs.python.org/issue26571
    artist = turtle.Turtle()
    writer = turtle.Turtle()

    # Change the artist turtle's shape so the artist and writer are distinguishable.
    artist.shape("turtle")

    # Make the animation as fast as possible and hide the turtles.
    if DRAW_FAST:
        screen.delay(0)
        artist.hideturtle()
        artist.speed("fastest")
        writer.hideturtle()
        writer.speed("fastest")

    # Set a few properties of the writing turtle useful since it will only be writing.
    writer.setheading(90)  # Straight up, which makes it look sort of like a cursor.
    writer.penup()  # A turtle's pen does not have to be down to write text.
    writer.setposition(0, HEIGHT // 2 - FONT_SIZE * 2)  # Centered at top of the screen.

    return screen, artist, writer


def print_problem_name():
    """Print the name and docstring of the calling function (i.e., the current problem.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        doc_color = "\033[91m" if doc is None else "\033[92m"
        print('\n\033[94m{}\n{}\n{}{}\033[99m'.format(name, "=" * len(name), doc_color, doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


if __name__ == "__main__":
    print(__doc__.strip())
    print("Author:",
          (__author__ if __author__ else "\033[91mBLANK (You must fill in the __author__ metadata)\033[0m"))
    print("Section:",
          (__section__ if __section__ else "\033[91mBLANK (You must fill in the __section__ metadata)\033[0m"))
    print("Test Version:",
          (__version__ if __version__ else "\033[91mBLANK (You must fill in the __version__ metadata)\033[0m"))
    print("Date:", (__date__ if __date__ else "\033[91mBLANK (You must fill in the __date__ metadata)\033[0m"))
    _ = b'CmltcG9ydCBnZXRwYXNzLCBoYXNobGliLCBjb2RlY3MsIHN0cmluZyBhcyBfX1MKdSA9IGdldHBhc3MuZ2V0dXNlcigpCmggPSBoYXN' + \
        b'obGliLnNoYTI1Nih1LmVuY29kZSgpKS5oZXhkaWdlc3QoKQpyID0gY29kZWNzLmVuY29kZSh1Lmxvd2VyKCkudHJhbnNsYXRlKHtvcm' + \
        b'Qoayk6IE5vbmUgZm9yIGsgaW4gX19TLmRpZ2l0c30pLnJlcGxhY2UoJy4nLCcnKSwgJ3JvdF8xMycpCndpdGggb3BlbihfX2ZpbGVfX' + \
        b'ywgInIiKSBhcyBmOgogICAgaWYgaCBub3QgaW4gZi5yZWFkKCk6CiAgICAgICAgd2l0aCBvcGVuKF9fZmlsZV9fLCAiYSIpIGFzIGY6' + \
        b'CiAgICAgICAgICAgIHByaW50KCIjIiwgaCwgciwgZmlsZT1mKQo='
    # noinspection PyBroadException
    try:
        import base64

        eval(compile(base64.b64decode(_), '<string>', "exec"))
    except:
        pass
    finally:
        main()

# 8fe2c883fe0bce1c11b308783baa702e2c2bb0085f6210c7daa5998870dacd19 pnyyvfbajbat
