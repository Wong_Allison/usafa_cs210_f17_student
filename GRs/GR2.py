#!/usr/bin/env python3
"""
GR2, CS210, Fall 2017
"""
import os
import random
import string
import easygui

# Fill in the metadata
__author__ = "Allison Wong"  # Your name. Ex: John Doe
__section__ = "M3"  # Your section. Ex: M1
__version__ = "A"  # Test version. Ex: A
__date__ = "16 Oct 2017"  # Today's date. Ex: 25 Dec 2017

""" The following code segments are provided for your reference.
    You are free to copy/paste them into your answers below.

    # Use an easygui.fileopenbox to get a filename from the user.
    filename = easygui.fileopenbox(default="../Data/*.txt")

    # Read the entire contents of a file into a single string.
    with open(filename, "r") as data_file:
        data_string = data_file.read()

    # Read the entire contents of a file into a list of strings, one per word.
    with open(filename, "r") as data_file:
        data_words = data_file.read().split()

    # Read the entire contents of a file into a list of strings, one per line.
    with open(filename, "r") as data_file:
        data_lines = data_file.read().splitlines()

    # Split a filename into its base and extension; build a new filename.
    base, ext = os.path.splitext(filename)
    output_filename = "{}_Output{}".format(base, ext)

    # Write a string to a file.
    with open(output_filename, "w") as output_file:
        output_file.write( "This is the output.\n" )
"""


# ======================================================================
# Main
# ======================================================================
def main():
    """ Comment and uncomment these lines as needed to work on each problem. """
    # problem1()
    # problem2()
    # problem3()
    # problem4()
    # problem5()
    problem6()


# ======================================================================
# Problem 1                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# ======================================================================
def problem1():
    """Problem 1 from the GR."""
    print_problem_name()

    # TODO 1b
    ticket = generate_lotto(24)
    winner = generate_lotto(24)

    count = 0
    for m in ticket:
        for n in winner:
            if m == n:
                count += 1

    print("Ticket = ", ticket)
    print("Winner = ", winner)
    print("Count = ", count)


# TODO 1a
def generate_lotto(upper):
    lotto = []
    for i in range(6):
        num = random.randint(1, upper)
        lotto.append(num)
    while len(set(lotto)) < 6:
        for m in lotto:
            for n in lotto:
                if m == n:
                    lotto.remove(n)
                    num = random.randint(1, upper)
                    lotto.append(num)
    return sorted(lotto)


# ======================================================================
# Problem 2                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# ======================================================================
def problem2():
    """Problem 2 from the GR."""
    print_problem_name()

    board1 = [list("aa.bocc"),
              list("aa.bbcc"),
              list("qq.abb."),
              list("qqcaabb"),
              list("occaabb"),
              list("ccaacoo")]

    board2 = [list("q.c.oo.qb."),
              list("baa.boccbb"),
              list("baa.bbccba"),
              list("aqq.abb.ab"),
              list("aqqcaabbcq"),
              list("c.ccaabbcc"),
              list("cccaacooqc")]

    # TODO 2b
    print("Board 1, a: ", count_pattern(board1, "a"))
    print("Board 1, b: ", count_pattern(board1, "b"))
    print("Board 1, c: ", count_pattern(board1, "c"))

    print("Board 2, a: ", count_pattern(board2, "a"))
    print("Board 2, b: ", count_pattern(board2, "b"))
    print("Board 2, c: ", count_pattern(board2, "c"))


# TODO 2a
def count_pattern(lst, str):
    count = 0
    for i in range(len(lst)):
        for j in range(len(lst[i])):
            if i + 1 < len(lst) and j + 1 < len(lst[i]):
                if lst[i][j] == str and lst[i + 1][j] == str and lst[i + 1][j + 1] == str:
                    count += 1

    return count


# ======================================================================
# Problem 3                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# ======================================================================
def problem3():
    """Problem 3 from the GR."""
    print_problem_name()

    # TODO 3b
    filename = easygui.fileopenbox(default="../Data/*.txt")
    output_file = filename.replace(".txt", "_madlib.txt")
    nouns = ["sister", "book", "rabbit", "pictures"]
    convert_to_madlib(filename, output_file, nouns)


# TODO 3a
def convert_to_madlib(filename, nfilename, lst):
    with open(filename, "r") as f:
        words = f.read().split()
    with open(nfilename, "w") as w:
        for word in words:
            for noun in lst:
                if word.lower() == noun.lower():
                    word = "_noun_"
                elif word[len(word) - 3:].lower() == "ing":
                    word = "_verb_ing_"
            print(word, "", end="", file=w)


# ======================================================================
# Problem 4                                                 ____ / 5 pts
# Grades: All or nothing
# ======================================================================
def problem4():
    """Problem 4 from the GR."""
    print_problem_name()

    # TODO 4
    answer = "G E A B F C D"  # Put your answer here, eg, "L M N O P"
    print("Sequence:", answer)


# ======================================================================
# Problem 5                                                ____ / 20 pts
# Grades: A: 20, B: 16, C: 14, D: 12, F: 10
# ======================================================================
def problem5():
    """Problem 5 from the GR."""
    print_problem_name()

    # TODO 5b
    # Testing for Pass/Fail
    if "" == recursive_extract_vowels(""):
        print("Empty string:", recursive_extract_vowels(""), "Pass")
    else:
        print("Empty string:", recursive_extract_vowels(""), "Fail")

    if "uio" == recursive_extract_vowels("function"):
        print("Function:", recursive_extract_vowels("function"), "Pass")
    else:
        print("Function:", recursive_extract_vowels("Function"), "Fail")

    if "ouu" == recursive_extract_vowels("output"):
        print("output:", recursive_extract_vowels("output"), "Pass")
    else:
        print("output:", recursive_extract_vowels("output"), "Fail")

    if "oeio" == recursive_extract_vowels("conversions"):
        print("conversions:", recursive_extract_vowels("conversions"), "Pass")
    else:
        print("conversions:", recursive_extract_vowels("conversions"), "Fail")

    if "Ae" == recursive_extract_vowels("Apple"):
        print("Apple:", recursive_extract_vowels("Apple"), "Pass")
    else:
        print("Apple", recursive_extract_vowels("Apple"), "Fail")


# TODO 5a
def recursive_extract_vowels(str):
    vowels = ["a", "e", "i", "o", "u"]
    if len(str) <= 0:
        return ""
    else:
        if str[0].lower() in vowels:
            returned = str[0] + recursive_extract_vowels(str[1:])
        else:
            returned = "" + recursive_extract_vowels(str[1:])
    return returned


# ======================================================================
# Problem 6                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# ======================================================================
def problem6():
    """Problem 6 from the GR."""
    print_problem_name()

    # TODO 6c
    updates = load_updates("../Data/update_inventory.txt")
    inventory = {"apples": 1000, "grapes": 700}
    print("Before:", inventory)
    update_inventory(inventory, updates)
    print("After:", inventory)


# TODO 6b
def update_inventory(dic, lst):
    for thing in lst:
        if thing[0] not in dic:
            dic[thing[0]] = thing[1]
        else:
            dic[thing[0]] = dic[thing[0]] + thing[1]


# TODO 6a
def load_updates(filename):
    new_lst = []
    with open(filename, "r") as f:
        lines = f.read().splitlines()
    for line in lines:
        pairs = line.split()
        new_lst.append((pairs[0], int(pairs[1])))
    return new_lst


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def print_problem_name():
    """Print the name and docstring of the calling function (i.e., the current problem.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        doc_color = "\033[91m" if doc is None else "\033[92m"
        print('\n\033[94m{}\n{}\n{}{}\033[99m'.format(name, "=" * len(name), doc_color, doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


if __name__ == "__main__":
    print(__doc__.strip())
    print("Author:",
          (__author__ if __author__ else "\033[91mBLANK (You must fill in the __author__ metadata)\033[0m"))
    print("Section:",
          (__section__ if __section__ else "\033[91mBLANK (You must fill in the __section__ metadata)\033[0m"))
    print("Test Version:",
          (__version__ if __version__ else "\033[91mBLANK (You must fill in the __version__ metadata)\033[0m"))
    print("Date:", (__date__ if __date__ else "\033[91mBLANK (You must fill in the __date__ metadata)\033[0m"))
    _ = b'CmltcG9ydCBnZXRwYXNzLCBoYXNobGliLCBjb2RlY3MsIHN0cmluZyBhcyBfX1MKdSA9IGdldHBhc3MuZ2V0dXNlcigpCmggPSBoYXN' + \
        b'obGliLnNoYTI1Nih1LmVuY29kZSgpKS5oZXhkaWdlc3QoKQpyID0gY29kZWNzLmVuY29kZSh1Lmxvd2VyKCkudHJhbnNsYXRlKHtvcm' + \
        b'Qoayk6IE5vbmUgZm9yIGsgaW4gX19TLmRpZ2l0c30pLnJlcGxhY2UoJy4nLCcnKSwgJ3JvdF8xMycpCndpdGggb3BlbihfX2ZpbGVfX' + \
        b'ywgInIiKSBhcyBmOgogICAgaWYgaCBub3QgaW4gZi5yZWFkKCk6CiAgICAgICAgd2l0aCBvcGVuKF9fZmlsZV9fLCAiYSIpIGFzIGY6' + \
        b'CiAgICAgICAgICAgIHByaW50KCIjIiwgaCwgciwgZmlsZT1mKQo='
    # noinspection PyBroadException
    try:
        import base64

        eval(compile(base64.b64decode(_), '<string>', "exec"))
    except:
        pass
    finally:
        main()

# 8fe2c883fe0bce1c11b308783baa702e2c2bb0085f6210c7daa5998870dacd19 pnyyvfbajbat
