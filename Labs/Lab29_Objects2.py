#!/usr/bin/env python3
"""
PUT YOUR DESCRIPTION OF THIS FILE HERE
CS 210, Introduction to Programming
"""

__author__ = "Allison Wong"
__instructor__ = "Lt Col Harder"
__date__ = "1 Nov 2017"
__documentation__ = """None"""  # No USAFA documentation statement required for Labs

"""
Preparation
-	Read Lesson 29: Classes & Objects from our online textbook

Lesson Objectives
-	Enhance the simple classes with multiple attributes from previous lesson
-	Write and use a constructor with parameters
	Write and use additional class methods


"""

import math
import random
import turtle

# Define several useful constants to be used by the Turtle graphics.
WIDTH = 960  # Usually 720, 960, 1024, 1280, 1600, or 1920.
HEIGHT = WIDTH * 9 // 16  # Produces the eye-pleasing 16:9 HD aspect ratio.
MARGIN = WIDTH // 30  # Somewhat arbitrary value, but it looks nice.
FONT_SIZE = MARGIN // 2  # Somewhat arbitrary value, but it looks nice.
DRAW_FAST = False  # Set to True for fast, non-animated turtle movement.

COLORS = ["red", "green", "blue", "yellow", "cyan", "magenta", "white", "black"]


def main():
    """Main program to test solutions for each exercise."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print(__author__, __doc__)

    # Call each individual exercise; comment/un-comment these lines as you work.
    # exercise0()
    # exercise1()
    exercise2()


def exercise0():
    """
    Point Class Demo – Work with a partner to read, discuss, and understand the given code.
    Be sure to ask other classmates and/or your instructor if anything is unclear.

    a.	In the space "TODO 0a", is a definition of the Point class' __init__ method with
        two additional parameters for the x and y attributes.  Note the parameter names can
        be the same as the attribute names since they are distinguished by the object reference
        self, which is always the first parameter to a class method and is an explicit reference
        to the object itself.  Also note the parameters are given default values of zero.
        Read, discuss, and understand this code.

    b.	In the space "TODO 0b", is code that creates a list of Point objects, passing actual
        parameter values for the x and y attributes to the Point constructor.
        Read, discuss, and understand this code.

    c.	In the space "TODO 0c", is code that implements a draw method in the Point object.
        The method has two parameters; the first is self and is the explicit reference to the
        object itself, and the second is a turtle to be used to draw the Point object.
        Read, discuss, and understand this code.

    d.	In the space "TODO 0d", is code that loops through the previously created list of
        Point objects, calling the draw method of each.  Notice there is only one actual
        parameter in the line of code, p.draw( artist ).  The object instance p is implicitly
        passed as the first actual parameter and is assigned to the formal parameter self.
        Read, discuss, and understand this code.
    """
    print_exercise_name()

    # Create the turtle screen and two turtles (leave this as the first line).
    screen, artist, writer = turtle_setup()
    writer.write("Creating and drawing Point objects...",
                 align="center", font=("Times", FONT_SIZE, "bold"))

    # TODO 0b: Read, discuss, and understand the following code.
    points = []  # An empty list to be filled with Point objects.
    y = HEIGHT // 4  # Start the y-coordinate one-quarter screen height above the x-axis.
    # Loop through evenly spaced x-coordinates.
    for x in range(-WIDTH // 2 + MARGIN, WIDTH // 2 + MARGIN, (WIDTH - MARGIN * 2) // 8):
        p = Point(x, y)  # Use the values of x and y to create a Point object.
        points.append(p)  # Appends the point to the list of point objects.
        y *= -1  # Modify y so the points alternate above and below the x-axis.

    # TODO 0d: Read, discuss, and understand the following code.
    # Loop through the list of Point objects and tell each to draw itself.
    for p in points:
        # Tell the Point object to draw itself using the artist turtle.
        p.draw(artist)

    # Wait for the user to click before closing the window (leave this as the last line).
    artist.home()
    screen.exitonclick()


class Point:
    """Point class for representing (x,y) coordinates."""

    # TODO 0a: Read, discuss, and understand the following code.
    def __init__(self, x=0, y=0):
        """Create a new Point with the given x and y values.

        :param int x: The x-coordinate; default is zero.
        :param int y: The y-coordinate; default is zero.
        """
        # Assign the x and y values passed as parameters as attributes of self.
        self.x = x
        self.y = y
        self.size = 4

    # TODO 0c: Read, discuss, and understand the following code.
    def draw(self, art):
        """Draw this Point object using the given turtle.

        :param turtle.Turtle art: The turtle to use to draw this Point object.
        :return: None
        """
        # Use the self object's x and y values to set the heading.
        art.setheading(art.towards(self.x, self.y))
        # Use the self object's x and y values to move the turtle.
        art.setposition(self.x, self.y)
        # Draw a dot at the point.
        art.dot(self.size)


def exercise1():
    """
    Spot Class – In this exercise you will enhance the Spot class from our previous lesson.

n

            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |  Creating and drawing Spot objects... |
            |                                       |
            |                                   o   |
            |                               o       |
            |                           o           |    (picture the circles with different colors)
            |                       o               |
            |                   o                   |
            |               o                       |
            |           o                           |
            |       o                               |
            |   o                                   |
            |                                       |
            +---------------------------------------+

c.	Finally, modify the draw method in the Spot class to add an artistic flair to the spots.
    One possibility is to have four solid circles drawn in a plus shape and a fifth circle traced
    on top with a different color.  Notice the encapsulation of the drawing of a Spot object
    in this one method; why is this good design?
    """
    print_exercise_name()

    # Create the turtle screen and two turtles (leave this as the first line).
    screen, artist, writer = turtle_setup()
    writer.write("Creating and drawing Spot objects...",
                 align="center", font=("Times", FONT_SIZE, "bold"))
    writer.ht()
    artist.speed("fast")

    # TODO 1b: In the space below, use the class as described
    spots = []
    for s in range(9):
        spot = Spot(int((-WIDTH / 2) + (MARGIN * (s + 1))), int((-HEIGHT / 2) + (MARGIN * (s + 1))))
        artist.color(spot.color)
        spot.draw(artist)
        spots.append(spot)
    artist.ht()

    # Wait for the user to click before closing the window (leave this as the last line).
    artist.home()
    screen.exitonclick()


# TODO 1a: In the space below this comment, write the class as described
class Spot:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y
        self.color = random.choice(COLORS)

    def draw(self, art):
        art.setposition(self.x, self.y)
        art.dot(8)


def exercise2():
    """
    Raindrop Class – In this exercise you will enhance the Raindrop class from our previous lesson.

    a.	In the space "TODO 2a", define a Raindrop class with an __init__ method that defines
        x and y attributes and also a radius attribute.  Use the same random values from our
        previous lab; the __init__ method in the Raindrop class does not need to accept additional
        parameters (i.e., Raindrops are still random).

        Include a draw method in the Raindrop class.  This method should accept one additional
        parameter, beyond the explicit self parameter, that is a turtle object to be used to
        draw the Raindrop.

        Include an area method in the Raindrop class.  This method does not accept any additional
        parameters beyond the explicit self parameter.  The method calculates and returns the area
        of the Raindrop object.

        Include an overlaps method in the Raindrop class.  This method accepts one additional
        parameter that is a second Raindrop object.  It is quite common to have a parameter that
        is a second instance of the same class; such a parameter is usually named other.  The function
        determines if the Raindrop object referenced by the explicit self parameter overlaps the
        Raindrop object referenced by the other parameter, returning True if it does, False if
        it does not.

        Include an expand method in the Raindrop class.  This method accepts two additional
        parameters; a second Raindrop object (named other) and a turtle to draw the expanded
        Raindrop.  The method updates the radius of the Raindrop object referenced by the explicit
        self parameter such that its area is increased by the area of the other object.  The method
        should then call the draw method of the Raindrop object referenced by the explicit self
        parameter, passing the turtle to do the drawing.

    b.	In the space "TODO 2b", re-write the same raindrop application from our previous lesson
        using the new methods in the Raindrop class.
    """
    print_exercise_name()

    # Create the turtle screen and two turtles (leave this as the first line).
    screen, artist, writer = turtle_setup()
    writer.write("Creating and drawing Raindrop objects...",
                 align="center", font=("Times", FONT_SIZE, "bold"))

    # Make the artist turtle a blue circle for this application.
    # artist.color( 'blue' )
    # artist.shape( "circle" )

    # TODO 2b: In the space below, use the class as described
    raindrops = []
    area = 0.0
    while area < WIDTH*HEIGHT:
        new_drop = Raindrop()
        print("Drawing new drop {}.".format(new_drop), flush=True)
        new_drop.draw(artist)

        raindrops.append(new_drop)
        area += new_drop.area()

        for old_drop in raindrops[:-1]:
            if new_drop.overlaps(old_drop):
                print("Expanding drop {}.".format(old_drop), flush=True)
                old_drop.expand(new_drop, artist)
                area += new_drop.area()

    # Wait for the user to click before closing the window (leave this as the last line).
    artist.home()
    screen.exitonclick()


# TODO 2a: In the space below this comment, write the class as described
class Raindrop:
    def __init__(self, x=None, y=None, radius=None):
        self.x = x if x is not None else random.randint(-WIDTH//2 + MARGIN + self.r, WIDTH//2 - MARGIN - self.r)
        self.y = y if y is not None else random.randint(-HEIGHT//2 + MARGIN + self.r, HEIGHT//2 - MARGIN - self.r)
        self.radius = r if r is not None else random.randint(MARGIN, MARGIN * 2)

    def draw(self, art):
        art.setheading(art.towards(self.x, self.y))
        art.setposition(self.x, self.y)
        art.dot(self.r * 2, "blue")

    def area(self):
        return math.pi * self.r ** 2

    def overlaps(self, other):
        return math.hypot(self.x - other.x, self.y - other.y) <= self.r + other.r

    def expand(self, other, art):
        self.r = math.sqrt(self.r ** 2 + other.r ** 2)
        self.draw(art)



"""
Challenge Exercises:

1.	Complete unfinished exercises from any previous lab; in particular, the Dictionaries lab.
"""


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def turtle_setup():
    """Setup the turtle environment with a screen and two turtles, one for drawing and one for writing.

    Using separate turtles for drawing and writing makes it easy to clear one or the other by
    doing artist.clear() or writer.clear() to clear only the drawing or writing, respectively.

    :return: The screen, a drawing turtle, and a writing turtle.
    :rtype: (turtle.Screen, turtle.Turtle, turtle.Turtle)
    """
    #  ___   ___     _  _  ___ _____    __  __  ___  ___ ___ _____   __
    # |   \ / _ \   | \| |/ _ \_   _|  |  \/  |/ _ \|   \_ _| __\ \ / /
    # | |) | (_) |  | .` | (_) || |    | |\/| | (_) | |) | || _| \ V /
    # |___/ \___/   |_|\_|\___/ |_|    |_|  |_|\___/|___/___|_|   |_|
    #  _____ _  _ ___ ___    ___ _   _ _  _  ___ _____ ___ ___  _  _
    # |_   _| || |_ _/ __|  | __| | | | \| |/ __|_   _|_ _/ _ \| \| |
    #   | | | __ || |\__ \  | _|| |_| | .` | (__  | |  | | (_) | .` |
    #   |_| |_||_|___|___/  |_|  \___/|_|\_|\___| |_| |___\___/|_|\_|
    #
    # Create the turtle graphics screen and set a few basic properties.
    screen = turtle.Screen()
    screen.setup(WIDTH, HEIGHT, MARGIN, MARGIN)
    screen.bgcolor("SkyBlue")

    # Create two turtles, one for drawing and one for writing.
    turtle.TurtleScreen._RUNNING = True  # Get around bug in v3.5.2 http://bugs.python.org/issue26571
    artist = turtle.Turtle()
    writer = turtle.Turtle()

    # Change the artist turtle's shape so the artist and writer are distinguishable.
    artist.shape("turtle")
    # Lift the artist's pen and slow it down to see the movements from object to object.
    artist.penup()
    artist.speed("slowest")

    # Make the animation as fast as possible and hide the turtles.
    if DRAW_FAST:
        screen.delay(0)
        artist.hideturtle()
        artist.speed("fastest")
        writer.hideturtle()
        writer.speed("fastest")

    # Set a few properties of the writing turtle useful since it will only be writing.
    writer.setheading(90)  # Straight up, which makes it look sort of like a cursor.
    writer.penup()  # A turtle's pen does not have to be down to write text.
    writer.setposition(0, HEIGHT // 2 - FONT_SIZE * 2)  # Centered at top of the screen.

    return screen, artist, writer


def print_exercise_name():
    """Print the name and docstring of the calling function (i.e., the current exercise.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        print('\n\033[94m{}\n{}\n\033[92m{}\033[99m'.format(name, "=" * len(name), doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
