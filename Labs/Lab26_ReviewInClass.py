#!/usr/bin/env python3
"""

Review in class for GR2.
"""

# String manipulation
# File I/O
# Lists
# Nested Lists
# Recursion
# Dictionaries
import json
import string


def load_word_groups(filename):
    with open(filename, "r") as f:
        words = f.read().split()  # splits whitespaces, into list of words
        # Think about punctuation and capitalization
    d = {}  # type: dict[str, list[str]]  # says what this dict will be
    for word in words:
        if len(word) > 0:
            first_letter = word[0].lower()
            if first_letter in string.ascii_letters:  # only continue if it is actual letter
                if first_letter not in d:  # Checks if first_letter is already key in dictionary
                    d[first_letter] = []  # if not, add the key to the dictionary, term is an empty list

                word = word.lower()
                new_word = ""
                for c in word:
                    if c in string.ascii_letters:
                        new_word = new_word + c  # Strips punctuation, only adds letters to added word
                word = new_word
                d[first_letter].append(word)  # then save the term to the key in the dictionary
    return d


d = load_word_groups("../Data/Address.txt")


# print(json.dumps(d, indent=2))
# print("Number of first letters:", len(d))


def summarize_data(filename, data):
    with open(filename, "w") as f:
        for letter in string.ascii_lowercase:
            print(letter.upper(), file=f)

            if letter in data:  # check if the letter has words in the address
                for word in sorted(set(data[letter])):  # set gets rid of duplicates, sorted sorts the words
                    print("\t", word, file=f)  # prints all the words under this letter

                    # for word in data.get(letter, []):  # Another way to check, if get returns None, default is []
                    #     print("\t", word, file=f)  # prints all the words under this letter


summarize_data("../Data/Address_summary.txt", d)


# Recursion
def change_pi(text):
    if len(text) <= 1:  # Base case
        return text
    else:
        # if text[0:1] == "pi":
        if text.startswith("pi"):
            return "3.14" + change_pi(text[2:])
        else:
            return text[0] + change_pi(text[1:])


text = "happiness"
print(change_pi(text))

with open("../Data/Address.txt", "r") as f:
    lines = f.read().splitlines()
for line in lines:
    print(change_pi(line))


# Recursion 2
def all_star(text):
    if len(text) <= 1:
        return text
    else:
        if text[0] in string.ascii_letters and text[1] in string.ascii_letters:
            return text[0] + "*" + all_star(text[1:])
        else:
            return text[0] + all_star(text[1:])


word = "hello world"
print(all_star(word))


# Recursion 3
def count_pairs(text):
    if len(text) <= 2:
        return 0
    else:
        if text[0] == text[2]:
            pair = 1
        else:
            pair = 0
        return pair + count_pairs(text[1:])


def count_pairs_loop(text):
    count = 0
    for i in range(len(text)-2):
        if text[i] == text[i+2]:
            count += 1
    return count

tests = [
    ("AxA", 1),
    ("AxAxA", 3),
    ("cat", 0),
    ("", 0)
]

for text, right_answer in tests:
    check = count_pairs(text)
    check_loop = count_pairs_loop(text)
    print(text, check, "Correct?", check == right_answer)
    print(text, check_loop, "Loop Correct?", check_loop == right_answer)
