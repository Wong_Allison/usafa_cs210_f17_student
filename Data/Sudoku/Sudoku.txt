Sudoku Puzzles
00. A sample, solved puzzle, data in 9x9 format.
01. A sample, solved puzzle, data in three lines.
02. A sample, solved puzzle, data in one long line.
03. A sample, solved puzzle, with unsolved version in 03b; rated as easy.
04. Full, invalid puzzle, errors in all of the 3x3 grids.
05. Full, invalid puzzle, error is swapped values in [5][5] and [6][6]; probably first discovered with two 2s in row 5.
06. Incomplete, invalid puzzle; error likely to be discovered in row 4 with two 2s.
07. Incomplete, invalid puzzle; error likely to be discovered in column 4 with two 8s.
08. Incomplete, invalid puzzle; error is two 1s in the middle 3x3 grid.
09. Incomplete, invalid puzzle; error is two 5s in the lower right 3x3 grid.
10. Incomplete, but valid and solvable; rated as very easy.
11. Incomplete, but valid and solvable; rated as medium.
12. Incomplete, but valid and solvable; rated as difficult.
