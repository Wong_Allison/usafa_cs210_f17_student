#!/usr/bin/env python3
"""
A game of object-orientated Tic Tac Toe. Data is encapsulated in instances of Player and TicTacToe.
CS 210, Introduction to Programming
"""

__author__ = "Allison Wong"  # Your name. Ex: John Doe
__section__ = "M3"  # Your section. Ex: M1
__instructor__ = "Lt Col Harder"  # Your instructor. Ex: Lt Col Doe
__date__ = "6 Nov 2017"  # Today's date. Ex: 25 Dec 2017
__documentation__ = """I consulted C3C Landwehr on object setter concepts """  # Multiple lines OK with triple quotes


def main():
    # Gets names and markers of players from the users
    first_name, first_marker = create_player()
    second_name, second_marker = create_player()

    # Creates instances of the two players
    player1 = Player(first_name, first_marker)
    player2 = Player(second_name, second_marker)

    # Checks if the marker names are too long, NoneType, or empty
    if first_marker is None or first_marker == "":
        player1.marker = "!"
    if second_marker is None or second_marker == "":
        player2.marker = "!"
    if len(first_marker) > 1:
        player1.marker = first_marker[0]
    if len(second_marker) > 1:
        player2.marker = second_marker[0]

    game = list()  # Creates a game list in which to store the TicTacToe instances
    game_num = 0  # An index to keep track of which game in the list it's at

    # Keeps running as long as no player has 2 wins
    while player1.wins < 2 and player2.wins < 2:
        # Creates new game and prints starting statement and board
        game.append(TicTacToe(player1, player2))
        print("\nBegin game #{}!".format(game_num + 1))
        print(board_string(game[game_num]))

        # Makes sure player1 starts odd-numbered games and player2 starts even-numbered games
        if (game_num + 1) % 2 == 0 and game[game_num].current_player == player1:
            game[game_num].switch_current(player2)
        elif (game_num + 1) % 2 == 1 and game[game_num].current_player == player2:
            game[game_num].switch_current(player1)

        # While there is no winner and there are still moves to be played, game continues
        while game[game_num].winner is None and game[game_num].turns_played < 9:
            # Prompts user for an move input
            move_num = int(
                input("{}'s turn. Where would you like to place {}?".format(game[game_num].current_player.name,
                                                                            game[game_num].current_player.marker)))
            play_game(game[game_num], move_num)  # Makes the play that the user inputted

        # Declares a tie if 9 moves have been played and there is no winner
        if game[game_num].turns_played == 9 and game[game_num].winner is None:
            print("It's a tie!")
            game[game_num].players[0].record_tie()
            game[game_num].players[1].record_tie()
        else:
            # Declares the winner of this round
            print("{} has won game {}!\n".format(game[game_num].winner.name, game_num + 1))

        # Prints player data after the round is over
        print(game[game_num].players[0])
        print(game[game_num].players[1])
        game_num += 1  # Increments the game number the players are on

    declare_winner(player1, player2)  # Declares the winner of the entire


def play_game(game, move_num):
    """
    Executes a move in the current game, re-prints, and then prints how many moves have been made thus far
    :param TicTacToe game: the current game instance
    :param int move_num: the number move the game is on
    """
    # Depending on what number the user chose, changes the TicTacToe instance's board
    if 1 <= move_num <= 3:
        game.play_move(1, move_num)
    elif 4 <= move_num <= 6:
        game.play_move(2, move_num - 3)
    elif 7 <= move_num <= 9:
        game.play_move(3, move_num - 6)
    print(board_string(game))  # Prints amended board

    # Prints the number of moves that have been made
    if game.turns_played == 1:
        print("1 move has been made.")
    else:
        print("{} moves have been made.".format(game.turns_played))


def declare_winner(player1, player2):
    """
    Determines who won 2 out of 3 games and declares them the winner
    :param Player player1: the player instance of player no. 1
    :param Player player2: the player instance of player no.2
    """
    if player1.wins == 2:
        print("{} won 2 out of 3 games!".format(player1.name))
    elif player2.wins == 2:
        print("{} won 2 out of 3 games!".format(player2.name))


def create_player():
    """
    Gathers a name and marker from the user for the Player
    :return: tuple of the new player's name and their marker
    :rtype: tuple
    """
    # Prompt the user for a name and a marker
    name = input("Please enter your player name.")
    marker = input("Please enter a tic-tac-toe marker.")

    return (name, marker)


def board_string(game):
    """
    Converts nested list into string suitable to print
    :param game: the TicTacToe object
    :return: string representing the game's board in its current state
    :rtype: str
    """
    # Turns nested list of a board into a printable string board
    board_lst = game.board
    board_str = "\n".join([' '.join([(str(a)) for a in sub_lst]) for sub_lst in board_lst]) + "\n"
    return board_str


class TicTacToe:
    """ A Tic Tac Toe game model """

    def __init__(self, player1, player2):
        """
        Initializes a new TicTacToe game.
        :param Player player1: the first player to take a turn
        :param Player player2: the second player to take a turn
        """
        self.__current_player = player1
        self.__players = (player1, player2)
        self.__winner = None
        self.__loser = None
        self.__turns_played = 0
        self.__board = [["1", "2", "3"], ["4", "5", "6"], ["7", "8", "9"]]

    @property
    def current_player(self):
        """
        Returns the current player
        :return: __current_player
        :rtype: Player
        """
        return self.__current_player

    @property
    def players(self):
        """
        Returns a tuple containing the two Players
        :return: __players
        :rtype: tuple
        """
        return self.__players

    @property
    def winner(self):
        """
        Returns the winning player
        :return: __winner
        :rtype: Player
        """
        return self.__winner

    @property
    def loser(self):
        """
        Returns the losing player
        :return: __loser
        :rtype: Player
        """
        return self.__loser

    @property
    def turns_played(self):
        """
        Returns the number of turns played
        :return: __turns_played
        :rtype: int
        """
        return self.__turns_played

    @property
    def board(self):
        """
        Returns the board
        :return: __board
        :rtype: list
        """
        return self.__board

    def play_move(self, row, col):
        """
        Given a row and col in the board, executes the move on the board
        Also checks for a winner and updates the current player after each move
        :param int row: 1-indexed row number in the board for the move
        :param int col: 1-indexed column number in the board for the move
        """
        # Checks if the spot chosen has already been played
        if self.player_at(row, col) is not None:
            print("Spot has already been played.")
            return
        else:
            # Puts player's marker in the specified spot
            if row == 1:
                self.__board[row - 1][col - 1] = self.__current_player.marker
            elif row == 2:
                self.__board[row - 1][col - 1] = self.__current_player.marker
            elif row == 3:
                self.__board[row - 1][col - 1] = self.__current_player.marker

        self.check_winner()  # Checks if there is a winner

        # Sets winner and loser of the game, if there is a winner
        if self.check_winner() is not None:
            self.__winner = self.check_winner()
            if self.__winner == self.__players[0]:
                self.__loser = self.__players[1]
                self.players[0].record_win()
                self.players[1].record_loss()
            else:
                self.__loser = self.__players[0]
                self.players[1].record_win()
                self.players[0].record_loss()
            print("{} won! {} lost.".format(self.__winner.name, self.__loser.name))

        self.add_turn()  # Increments the move count by 1

        # Switches current player after every move
        if self.__current_player == self.__players[0]:
            self.switch_current(self.__players[1])
        elif self.__current_player == self.__players[1]:
            self.switch_current(self.__players[0])

    def player_at(self, row, col):
        """
        Checks if a spot has a player's marker in it, and returns which player's it is.
        :param row: 1-indexed row number
        :param col: 1-indexed column number
        :return: the player whose marker is in that given spot
        :rtype: Player
        """
        # Determines what player, if any, is at a given location on the board
        if self.__board[row - 1][col - 1] == self.__players[0].marker:
            return self.__players[0]
        elif self.__board[row - 1][col - 1] == self.__players[1].marker:
            return self.__players[1]
        else:
            return None

    def add_turn(self):
        """
        Increments the turns-played count by one
        """
        self.__turns_played += 1

    def check_winner(self):
        """
        Checks the board for a winner, horizontally, vertically, and diagonally
        :return: if someone one, returns that Player object; if no winner, returns None
        :rtype: Player or str
        """
        # Check rows for winner
        for r in range(3):
            if self.board[r][0] == self.board[r][1] == self.board[r][2]:
                if self.board[r][0] == self.players[0].marker:
                    return self.players[0]
                else:
                    return self.players[1]

        # Check columns for winner
        for c in range(3):
            if self.board[0][c] == self.board[1][c] == self.board[2][c]:
                if self.board[0][c] == self.players[0].marker:
                    return self.players[0]
                else:
                    return self.players[1]

        # Check diagonals for winner
        if self.board[0][0] == self.board[1][1] == self.board[2][2]:
            if self.board[0][0] == self.players[0].marker:
                return self.players[0]
            else:
                return self.players[1]
        elif self.board[0][2] == self.board[1][1] == self.board[2][0]:
            if self.board[0][0] == self.players[0].marker:
                return self.players[0]
            else:
                return self.players[1]

    def switch_current(self, change_to):
        """
        Switches the player whose turn it is for the next move
        """
        self.__current_player = change_to


class Player:
    """ A player in the Tic Tac Toe game, with a readable and writeable name and marker. """

    def __init__(self, name, marker):
        """
        Initializes a new Player with a name and a marker.
        :param str name: the player's name
        :param str marker: the marker to use on the board
        """
        self.__name = name
        self.__marker = marker
        self.__wins = 0
        self.__losses = 0
        self.__ties = 0
        self.__games_played = 0

    @property
    def name(self):
        """
        Returns the player's name
        :return: __name
        :rtype: str
        """
        return self.__name

    @name.setter
    def name(self, name):
        """
        Returns the player's name
        :return: __name
        :rtype: str
        """
        self.__name = name

    @property
    def marker(self):
        """
        Returns the player's marker
        :return: __marker
        :rtype: str
        """
        return self.__marker

    @marker.setter
    def marker(self, marker):
        """
        Sets __marker to manipulated marker
        :param marker: marker passed down
        """
        if len(marker) > 1:
            self.__marker = marker[0]
        elif marker == "" or marker is None:
            self.__marker = "!"
        else:
            self.__marker = marker

    @property
    def wins(self):
        """
        Returns the number of wins
        :return: __wins
        :rtype: int
        """
        return self.__wins

    @property
    def losses(self):
        """
        Returns the number of losses
        :return: __losses
        :rtype: int
        """
        return self.__losses

    @property
    def ties(self):
        """
        Returns the number of ties
        :return: __ties
        :rtype: int
        """
        return self.__ties

    @property
    def games_played(self):
        """
        Returns the total number of games played
        :return: sum of wins, losses, and ties
        :rtype: int
        """
        return self.__wins + self.__losses + self.__ties

    def record_win(self):
        self.__wins += 1

    def record_loss(self):
        self.__losses += 1

    def record_tie(self):
        self.__ties += 1

    def __str__(self):
        """
        This method returns a string to print, containing the user's name, marker, wins, losses, and ties
        :return: a string containing the user's name, marker, wins, losses, and ties
        :rtype: str
        """
        return "{}: {}, {} wins, {} losses, {} ties".format(self.__name, self.__marker, self.__wins, self.__losses,
                                                            self.__ties)


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

if __name__ == "__main__":
    print(__doc__.strip())
    print("Author:", (__author__ if __author__ else "\033[91mBLANK (You must fill in the __author__ metadata)\033[0m"))
    print("Section:",
          (__section__ if __section__ else "\033[91mBLANK (You must fill in the __section__ metadata)\033[0m"))
    print("Instructor:",
          (__instructor__ if __instructor__ else "\033[91mBLANK (You must fill in the __instructor__ metadata)\033[0m"))
    print("Date:", (__date__ if __date__ else "\033[91mBLANK (You must fill in the __date__ metadata)\033[0m"))
    print("Documentation:", (
        __documentation__ if __documentation__.strip() else
        "\033[91mBLANK (You must fill in the __documentation__ metadata)\033[0m"))
    _ = b'CmltcG9ydCBnZXRwYXNzLCBoYXNobGliLCBjb2RlY3MsIHN0cmluZyBhcyBfX1MKdSA9IGdldHBhc3MuZ2V0dXNlcigpCmggPSBoYXN' + \
        b'obGliLnNoYTI1Nih1LmVuY29kZSgpKS5oZXhkaWdlc3QoKQpyID0gY29kZWNzLmVuY29kZSh1Lmxvd2VyKCkudHJhbnNsYXRlKHtvcm' + \
        b'Qoayk6IE5vbmUgZm9yIGsgaW4gX19TLmRpZ2l0c30pLnJlcGxhY2UoJy4nLCcnKSwgJ3JvdF8xMycpCndpdGggb3BlbihfX2ZpbGVfX' + \
        b'ywgInIiKSBhcyBmOgogICAgaWYgaCBub3QgaW4gZi5yZWFkKCk6CiAgICAgICAgd2l0aCBvcGVuKF9fZmlsZV9fLCAiYSIpIGFzIGY6' + \
        b'CiAgICAgICAgICAgIHByaW50KCIjIiwgaCwgciwgZmlsZT1mKQo='
    # noinspection PyBroadException
    try:
        import base64

        eval(compile(base64.b64decode(_), '<string>', "exec"))
    except:
        pass
    finally:
        main()
# 8fe2c883fe0bce1c11b308783baa702e2c2bb0085f6210c7daa5998870dacd19 pnyyvfbajbat
