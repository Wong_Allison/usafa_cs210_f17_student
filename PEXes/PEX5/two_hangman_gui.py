import tkinter as tk
import string
from functools import partial
from twoplayerhangman import TwoPlayerHangman
from player import Player
import easygui
from tkinter import messagebox


class HangmanTwoApp:
    """ A GUI that shows the hangman two-player screen """

    def __init__(self):
        # Player 1 Name
        self.player1window = tk.Tk()
        self.player1window.title("Player 1 Name")
        self.player_one_name = tk.StringVar()
        player_lbl = tk.Label(self.player1window, text="Player 1 Name:")
        player_lbl.grid(row=1, column=1)
        player_one_name = tk.Entry(self.player1window, textvariable=self.player_one_name)
        player_one_name.grid(row=1, column=2)

        # Enter Button
        enter_btn = tk.Button(self.player1window, text="Enter", command=self.secondary_window)
        enter_btn.grid(row=2, column=1, columnspan=2)

    def secondary_window(self):
        """
        Player 2 Name Window
        :return: None
        """
        self.__player_one = Player(self.player_one_name.get())
        self.player1window.destroy()
        self.player2window = tk.Tk()
        self.player2window.title("Player 2 Name")
        self.player_two_name = tk.StringVar()
        player_lbl = tk.Label(self.player2window, text="Player 2 Name:")
        player_lbl.grid(row=1, column=1)
        player_two_name = tk.Entry(self.player2window, textvariable=self.player_two_name)
        player_two_name.grid(row=1, column=2)

        # Tells us if we need to destroy previous window
        self.__number_of_attempts_to_put_word = 0

        # Enter Button
        enter_btn = tk.Button(self.player2window, text="Enter", command=self.third_window)
        enter_btn.grid(row=2, column=1, columnspan=2)

    def third_window(self):
        """
        Enter word to guess window
        :return: None
        """
        self.__player_two = Player(self.player_two_name.get())
        if self.__number_of_attempts_to_put_word == 0:
            self.player2window.destroy()
        self.wordwindow = tk.Tk()
        self.wordwindow.title("Word")
        self.word = tk.StringVar()
        if self.__number_of_attempts_to_put_word == 0:
            word_lbl = tk.Label(self.wordwindow, text="{}, Enter word".format(self.__player_one.name))
            word_lbl.grid(row=1, column=1)
        else:
            word_lbl = tk.Label(self.wordwindow, text="{}, use only letters in your word. Enter word again:".format(
                self.__player_one.name))
            word_lbl.grid(row=1, column=1)
        word_to_guess = tk.Entry(self.wordwindow, textvariable=self.word)
        word_to_guess.grid(row=1, column=2, columnspan=1, sticky=tk.E + tk.W)

        enter_btn = tk.Button(self.wordwindow, text="Enter", command=self.check_word)
        enter_btn.grid(row=2, column=1, columnspan=2)

    def check_word(self):
        """
        Checks if word is all letters
        :return: None
        """
        word = False
        self.__check_wordd = self.word.get().upper()
        self.wordwindow.destroy()
        for each in list(self.__check_wordd):
            if each not in string.ascii_uppercase or each == " ":
                word = False
                self.__number_of_attempts_to_put_word += 1
                break
            else:
                word = True
        if word == False:
            self.third_window()
        else:
            self.main_window()

    def main_window(self):
        """
        Opens main game window
        :return: None
        """
        self.__word_to_guess = self.__check_wordd.upper()
        self.__current_game = TwoPlayerHangman(self.__word_to_guess, self.__player_one.name, self.__player_two.name, 1)
        self.__current_player_index = 1
        self.window = tk.Tk()
        self.window.title("2-Player Hangman")

        self.canvas = None  # type: tk.Canvas
        self.create_widgets()

    def create_widgets(self):
        player1, player2 = self.__current_game.players

        hangman_lbl = tk.Label(self.window, text="Hangman")
        hangman_lbl.grid(row=0, column=0, columnspan=13)

        player_1_score_lbl = tk.Label(self.window, text="{}'s Score: {}".format(player1, self.__player_one.wins))
        player_1_score_lbl.grid(row=1, column=0, columnspan=3)

        player_2_score_lbl = tk.Label(self.window, text="{}'s Score: {}".format(player2, self.__player_two.wins))
        player_2_score_lbl.grid(row=1, column=10, columnspan=3)

        if self.__current_player_index == 0:
            self.guesser = self.__current_game.players[0]
        elif self.__current_player_index == 1:
            self.guesser = self.__current_game.players[1]
        guessing_lbl = tk.Label(self.window, text="{} is guessing.".format(self.guesser))
        guessing_lbl.grid(row=1, column=5, columnspan=3)

        # Canvas
        self.canvas = tk.Canvas(self.window, bg="white")
        self.canvas.grid(row=2, column=0, columnspan=13, rowspan=3,
                         sticky=tk.N + tk.S + tk.E + tk.W)
        self.create_canvas()

        # keyboard at bottom
        letters = string.ascii_uppercase
        for r in range(5, 7):
            for c in range(13):
                p = partial(self.board_clicked, r, c)
                if r == 5:
                    letter = letters[c]
                else:
                    letter = letters[c + 13]
                btn = tk.Button(self.window,
                                command=p,
                                width=1, height=1,
                                font=("Arial", 18), text=letter)
                btn.grid(row=r, column=c, sticky=tk.W + tk.E)

    def letters_in_world_drawing(self, letter):
        """
        Draws body parts of stick figure on gallow and draws letters on top of blanks
        :param letter: Letter the user guessed
        :return: None
        """
        self.__current_game.play_move(letter)
        x_point_1 = 237
        number_of_letters_in_word = len(list(self.__current_game.guessed_word))
        if number_of_letters_in_word % 2 == 1:
            x_point_1 = x_point_1 - 15 - (((number_of_letters_in_word - 1) / 2) * 26)
            for each in (list(self.__current_game.guessed_word)):
                if letter == each:
                    self.canvas.create_text(x_point_1 + 13, 285, text=letter, font=("Arial", 14))
                x_point_1 = x_point_1 + 26

        else:
            x_point_1 = x_point_1 - 2 - ((number_of_letters_in_word / 2) * 26)
            for each in (list(self.__current_game.guessed_word)):
                if letter == each:
                    self.canvas.create_text(x_point_1 + 12, 285, text=letter, font=("Arial", 14))
                x_point_1 = x_point_1 + 26

        if len(self.__current_game.incorrect_guesses) == 1:
            # Head
            self.canvas.create_oval(150, 100, 200, 150)
        elif len(self.__current_game.incorrect_guesses) == 2:
            # Body
            self.canvas.create_line(175, 150, 175, 200)
        elif len(self.__current_game.incorrect_guesses) == 3:
            # Right Arm
            self.canvas.create_line(175, 175, 200, 150)
        elif len(self.__current_game.incorrect_guesses) == 4:
            # Left Arm
            self.canvas.create_line(175, 175, 150, 150)
        elif len(self.__current_game.incorrect_guesses) == 5:
            # Right Leg
            self.canvas.create_line(175, 200, 200, 225)
        elif len(self.__current_game.incorrect_guesses) == 6:
            # Left Leg
            self.canvas.create_line(175, 200, 150, 225)
            # X-Eyes-Left
            self.canvas.create_line(160, 110, 170, 120)
            self.canvas.create_line(170, 110, 160, 120)
            # X-Eyes- Right
            self.canvas.create_line(190, 110, 180, 120)
            self.canvas.create_line(180, 110, 190, 120)

        if self.__current_game.winner is not None:
            self.create_widgets()
            if self.__current_game.winner == self.__current_game.players[0]:
                self.__player_one.record_wins()
            else:
                self.__player_two.record_wins()

            play_again_btn = tk.messagebox.askyesno("Play Again?",
                                                    "{} Wins! The word was {}. Do you want to play again?".format(
                                                        self.__current_game.winner, self.__word_to_guess))

            if play_again_btn is True:
                self.play_again()

            elif play_again_btn is False:
                self.window.destroy()

    def board_clicked(self, r, c):
        """Called when a button representing the board is clicked.
        :param int r: zero-indexed row
        :param int c: zero-indexed column
        """
        letters = string.ascii_uppercase
        if r == 5:
            letter = letters[c]
        else:
            letter = letters[c + 13]
        gray_btn = tk.Button(self.window, width=1, height=1,
                             font=("Arial", 18), text=letter, bg="Black")
        gray_btn.grid(row=r, column=c, sticky=tk.W + tk.E)
        self.letters_in_world_drawing(letter)

    def create_canvas(self):
        # Hangman outline
        self.canvas.create_line(175, 100, 175, 50)
        self.canvas.create_line(175, 50, 300, 50)
        self.canvas.create_line(300, 50, 300, 250)
        self.canvas.create_line(175, 250, 350, 250)

        # Create the blanks for the letters
        x_point_1 = 237
        number_of_letters_in_word = len(list(self.__current_game.guessed_word))
        if number_of_letters_in_word % 2 == 1:
            x_point_1 = x_point_1 - 15 - (((number_of_letters_in_word - 1) / 2) * 26)
            x_point_2 = x_point_1 + 22
            for each in range(number_of_letters_in_word):
                self.canvas.create_line(x_point_1, 300, x_point_2, 300)
                x_point_1 = x_point_1 + 26
                x_point_2 = x_point_1 + 22

        else:
            x_point_1 = x_point_1 - 2 - ((number_of_letters_in_word / 2) * 26)
            x_point_2 = x_point_1 + 22
            for each in range(number_of_letters_in_word):
                self.canvas.create_line(x_point_1, 300, x_point_2, 300)
                x_point_1 = x_point_1 + 26
                x_point_2 = x_point_1 + 22

    def quit_clicked(self):
        self.window.destroy()

    def play_again(self):

        self.__word_to_guess = easygui.enterbox(
            "{}, Enter word".format(self.__current_game.players[self.__current_player_index])).upper()

        if self.__current_player_index == 0:
            self.__current_player_index = 1
        elif self.__current_player_index == 1:
            self.__current_player_index = 0

        self.__current_game = TwoPlayerHangman(self.__word_to_guess, self.__player_one.name, self.__player_two.name,
                                               self.__current_player_index)

        self.create_widgets()
