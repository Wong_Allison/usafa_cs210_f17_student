import tkinter as tk
import string
from functools import partial
from oneplayerhangman import OnePlayerHangman
from player import Player
import random
from tkinter import messagebox


class HangmanOneApp:
    """ A GUI that shows the hangman one-player screen """

    def __init__(self):

        # Find player name window
        self.new_box = tk.Tk()
        self.new_box.title("Player Name")
        self.player_name = tk.StringVar()
        player_lbl = tk.Label(self.new_box, text="Player Name:")
        player_lbl.grid(row=1, column=1)
        player_name_box = tk.Entry(self.new_box, textvariable=self.player_name)
        player_name_box.grid(row=1, column=2)

        # Choose random word from text file
        with open("./words_alpha.txt") as data_file:
            self.__data = data_file.read().splitlines()
        self.__word_to_guess = str(random.choice(self.__data)).upper()
        self.__current_game = OnePlayerHangman(self.__word_to_guess, self.player_name)

        # Enter button for player name window
        self.enter_btn = tk.Button(self.new_box, text="Enter", command=self.main_window)
        self.enter_btn.grid(row=2, column=1, columnspan=2)

    def main_window(self):
        """
        Destroys player name window and opens up game window
        :return: None
        """

        self.__player = Player(self.player_name.get())
        self.new_box.destroy()
        self.window = tk.Tk()
        self.window.title("1-Player Hangman")

        self.canvas = None  # type: tk.Canvas
        self.create_widgets()

    def create_widgets(self):
        hangman_lbl = tk.Label(self.window, text="Hangman", font=("Arial", 36))
        hangman_lbl.grid(row=0, column=0, columnspan=13)

        # Canvas
        self.canvas = tk.Canvas(self.window, bg="white")
        self.canvas.grid(row=2, column=0, columnspan=13, rowspan=3,
                         sticky=tk.N + tk.S + tk.E + tk.W)
        self.create_canvas()

        # Keyboard Buttons
        letters = string.ascii_uppercase
        for r in range(5, 7):
            for c in range(13):
                p = partial(self.board_clicked, r, c)
                if r == 5:
                    letter = letters[c]
                else:
                    letter = letters[c + 13]
                btn = tk.Button(self.window,
                                command=p,
                                width=1, height=1,
                                font=("Arial", 18), text=letter)
                btn.grid(row=r, column=c, sticky=tk.W + tk.E)

        quit_btn = tk.Button(self.window, text="Quit", font=("Arial", 14), command=self.quit_clicked)
        quit_btn.grid(row=7, column=5, columnspan=3)

    def letters_in_world_drawing(self, letter):
        """
        Draws letters on top of blanks and hangman stick figure
        :param letter: Letter that player chooses
        :return: None
        """
        self.__current_game.play_move(letter)
        x_point_1 = 237
        number_of_letters_in_word = len(list(self.__current_game.guessed_word))
        if number_of_letters_in_word % 2 == 1:
            x_point_1 = x_point_1 - 15 - (((number_of_letters_in_word - 1) / 2) * 26)
            for each in (list(self.__current_game.guessed_word)):
                if letter == each:
                    self.canvas.create_text(x_point_1 + 12, 285, text=letter, font=("Arial", 14))
                x_point_1 = x_point_1 + 26

        else:
            x_point_1 = x_point_1 - 2 - ((number_of_letters_in_word / 2) * 26)
            for each in (list(self.__current_game.guessed_word)):
                if letter == each:
                    self.canvas.create_text(x_point_1 + 12, 285, text=letter, font=("Arial", 14))
                x_point_1 = x_point_1 + 26

        # all body parts of stick figure
        if len(self.__current_game.incorrect_guesses) == 1:
            # Head
            self.canvas.create_oval(150, 100, 200, 150)
        elif len(self.__current_game.incorrect_guesses) == 2:
            # Body
            self.canvas.create_line(175, 150, 175, 200)
        elif len(self.__current_game.incorrect_guesses) == 3:
            # Right Arm
            self.canvas.create_line(175, 175, 200, 150)
        elif len(self.__current_game.incorrect_guesses) == 4:
            # Left Arm
            self.canvas.create_line(175, 175, 150, 150)
        elif len(self.__current_game.incorrect_guesses) == 5:
            # Right Leg
            self.canvas.create_line(175, 200, 200, 225)
        elif len(self.__current_game.incorrect_guesses) == 6:
            # Left Leg
            self.canvas.create_line(175, 200, 150, 225)
            # X-Eyes-Left
            self.canvas.create_line(160, 110, 170, 120)
            self.canvas.create_line(170, 110, 160, 120)
            # X-Eyes- Right
            self.canvas.create_line(190, 110, 180, 120)
            self.canvas.create_line(180, 110, 190, 120)

        # Player Won
        if self.__current_game.win_or_lose() is True:
            play_again_btn = tk.messagebox.askyesno("Play Again?", "You win, {}. Do you want to play again?".format(
                self.player_name.get()))
            if play_again_btn is False:
                self.quit_clicked()
            elif play_again_btn is True:
                self.play_again()

        # Player Lost
        elif self.__current_game.win_or_lose() is False:
            play_again_btn = tk.messagebox.askyesno("Play Again?", "You lose, {}. The correct answer was {}. "
                                                                   "Do you want to play again?".format(
                self.player_name.get(), self.__word_to_guess))
            if play_again_btn is False:
                self.quit_clicked()
            elif play_again_btn is True:
                self.play_again()

    def board_clicked(self, r, c):
        """Called when a button representing the board is clicked.
        :param int r: zero-indexed row
        :param int c: zero-indexed column
        """
        letters = string.ascii_uppercase
        if r == 5:
            letter = letters[c]
        else:
            letter = letters[c + 13]
        # Blacks out letters used
        gray_btn = tk.Button(self.window, width=1, height=1,
                             font=("Arial", 18), text=letter, bg="Black")
        gray_btn.grid(row=r, column=c, sticky=tk.W + tk.E)
        self.letters_in_world_drawing(letter)

    def create_canvas(self):
        # Hangman outline
        self.canvas.create_line(175, 100, 175, 50)
        self.canvas.create_line(175, 50, 300, 50)
        self.canvas.create_line(300, 50, 300, 250)
        self.canvas.create_line(175, 250, 350, 250)

        # Create the blanks for the letters
        x_point_1 = 237
        number_of_letters_in_word = len(list(self.__current_game.guessed_word))
        if number_of_letters_in_word % 2 == 1:
            x_point_1 = x_point_1 - 15 - (((number_of_letters_in_word - 1) / 2) * 26)
            x_point_2 = x_point_1 + 22
            for each in range(number_of_letters_in_word):
                self.canvas.create_line(x_point_1, 300, x_point_2, 300)
                x_point_1 = x_point_1 + 26
                x_point_2 = x_point_1 + 22

        else:
            x_point_1 = x_point_1 - 2 - ((number_of_letters_in_word / 2) * 26)
            x_point_2 = x_point_1 + 22
            for each in range(number_of_letters_in_word):
                self.canvas.create_line(x_point_1, 300, x_point_2, 300)
                x_point_1 = x_point_1 + 26
                x_point_2 = x_point_1 + 22

    def quit_clicked(self):
        self.window.destroy()

    def play_again(self):
        self.__word_to_guess = str(random.choice(self.__data)).upper()
        self.__current_game = OnePlayerHangman(self.__word_to_guess, self.__player.name)

        self.create_widgets()
