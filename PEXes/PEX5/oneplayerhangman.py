"""The One Player Hangman class"""


class OnePlayerHangman:
    """
    An instance of a game of one-player hangman.
    """
    def __init__(self, guessed_word, player1):
        self.__word = guessed_word
        self.__player = player1
        self.__letters_guessed = []
        self.__correct_guesses = []
        self.__incorrect_guesses = []

    @property
    def guessed_word(self):
        """
        Returns the word to be guessed chosen randomly from file of words.
        :return: the word to be guessed
        :rtype: str
        """
        return self.__word

    @property
    def player(self):
        """
        Returns the player instance of the one-player game
        :return: the Player instance
        :rtype: Player
        """
        return self.__player

    @property
    def letters_guessed(self):
        """
        Readable list of letters
        :return: a list of the letters that have been guessed
        :rtype: list
        """
        return self.__letters_guessed

    @property
    def correct_guesses(self):
        """
        Readable list of letters
        :return: a list of the letters that have been guessed correctly
        :rtype: list
        """
        return self.__correct_guesses

    @property
    def incorrect_guesses(self):
        """
        Readable list of letters
        :return: a list of the letters that have been guessed incorrectly
        :rtype: list
        """
        return self.__incorrect_guesses

    def play_move(self, guessed_letter):
        """
        Executes a move in the game, once a letter has been guessed.
        :param guessed_letter: the letter that the player guessed
        """
        for letter in list(self.__word):
            if guessed_letter == letter:
                self.__correct_guesses.append(guessed_letter)
            elif guessed_letter not in (list(self.__word)) and guessed_letter not in self.__incorrect_guesses:
                self.__incorrect_guesses.append(guessed_letter)
        self.__letters_guessed.append(guessed_letter)

    def win_or_lose(self):
        """
        Checks if the game has ended as a win or a loss
        :return: whether the game has ended as a win or loss
        :rtype: bool
        """
        if sorted(self.__correct_guesses) == sorted(list(self.__word)):
            return True
        elif len(self.__incorrect_guesses) >= 6:
            return False
