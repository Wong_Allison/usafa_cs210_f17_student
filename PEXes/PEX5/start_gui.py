import tkinter as tk


class StartApp:
    """ A GUI that shows the start screen """

    def __init__(self):
        self.window = tk.Tk()
        self.window.title("Hangman Start")
        self.window.configure(bg="Black")

        self.__game_mode = 0

        self.canvas = None  # type: tk.Canvas
        self.create_widgets()

    @property
    def game_mode(self):
        """
        Return which mode you want: 1-Player or 2-Player
        :return: Number of the mode you want
        :rtype: int
        """
        return self.__game_mode

    def create_widgets(self):
        hangman_lbl = tk.Label(self.window, text="Welcome to Hangman", font=("Arial", 20), bg="black", fg="white")
        hangman_lbl.grid(row=0, column=0, columnspan=10)

        one_player_btn = tk.Button(self.window, text=" 1 Player", command=self.player1_clicked)
        one_player_btn.grid(row=3, column=1, columnspan=2)

        two_players_btn = tk.Button(self.window, text="2 Players", command=self.player2_clicked)
        two_players_btn.grid(row=3, column=6, columnspan=2)

        # Canvas
        self.canvas = tk.Canvas(self.window, bg="white")
        self.canvas.grid(row=1, column=0, columnspan=9,
                         sticky=tk.N + tk.S + tk.E + tk.W)
        self.create_canvas()

    def player1_clicked(self):
        """
        When the player1 button is clicked, start window is closed.
        """
        self.__game_mode = 1
        self.window.destroy()

    def player2_clicked(self):
        """
        When the player2 button is clicked, start window is closed.
        """
        self.__game_mode = 2
        self.window.destroy()

    def create_canvas(self):
        """
        Draws Hangman Outline and Blanks for number of letters in word
        """

        # Hangman outline
        self.canvas.create_line(175, 100, 175, 50)
        self.canvas.create_line(175, 50, 300, 50)
        self.canvas.create_line(300, 50, 300, 250)
        self.canvas.create_line(175, 250, 350, 250)

        # Head
        self.canvas.create_oval(150, 100, 200, 150)

        # Body
        self.canvas.create_line(175, 150, 175, 200)

        # Right Arm
        self.canvas.create_line(175, 175, 200, 150)

        # Left Arm
        self.canvas.create_line(175, 175, 150, 150)

        # Right Leg
        self.canvas.create_line(175, 200, 200, 225)

        # Left Leg
        self.canvas.create_line(175, 200, 150, 225)

        # X-Eyes-Left
        self.canvas.create_line(160, 110, 170, 120)
        self.canvas.create_line(170, 110, 160, 120)

        # X-Eyes- Right
        self.canvas.create_line(190, 110, 180, 120)
        self.canvas.create_line(180, 110, 190, 120)
