#!/usr/bin/env python3
"""
Pex 5 HANGMAN
CS 210, Introduction to Programming
"""

__author__ = "Tricia Dang and Allison Wong. Please grade Allison's"
__section__ = "M3"
__instructor__ = "Lt Col Harder"
__date__ = "11 Nov 2017"
__documentation__ = """C3C Kuhn told me if we wanted to use the player class, we have to do self.__player in order
to use it in the rest of my functions.He also assisted us in pulling text from a gui entry box."""

from start_gui import StartApp
from two_hangman_gui import HangmanTwoApp
from one_hangman_gui import HangmanOneApp


def main():
    app = StartApp()
    app.window.mainloop()

    if app.game_mode == 1:
        app = HangmanOneApp()
        app.new_box.mainloop()
    elif app.game_mode == 2:
        app = HangmanTwoApp()
        app.player1window.mainloop()
    else:
        print("Error")


if __name__ == '__main__':
    main()
