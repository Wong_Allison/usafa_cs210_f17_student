"The Two Player Hangman class"""


class TwoPlayerHangman:
    """
    An instance of a game of two-player hangman.
    """
    def __init__(self, guessed_word, player1, player2, current_guesser_index):
        self.__players = (player1, player2)
        self.__current_guesser_index = current_guesser_index
        self.__letters_guessed = []
        self.__correct_guesses = []
        self.__incorrect_guesses = []
        self.__word = guessed_word
        self.__winner = None
        self.__loser = None

    @property
    def guessed_word(self):
        """
        Readable word to be guessed
        :return: the word to be guessed in this game
        :rtype: str
        """
        return self.__word

    @property
    def current_guesser(self):
        """
        Readable current player guessing
        :return: the Player currently guessing
        :rtype: Player
        """
        return self.__players[self.__current_guesser_index]

    @property
    def players(self):
        """
        Readable tuple of the two players
        :return: the two player objects
        :rtype: tuple
        """
        return tuple(self.__players)

    @property
    def letters_guessed(self):
        """
        Readable list of letters guessed
        :return: the letters that have been guessed
        :rtype: list
        """
        return self.__letters_guessed

    @property
    def correct_guesses(self):
        """
        Readable list of letters guessed correctly
        :return: the letters that have been guessed correctly
        :rtype: list
        """
        return self.__correct_guesses

    @property
    def incorrect_guesses(self):
        """
        Readable list of letters guessed incorrectly
        :return: the letters that have been guessed incorrectly
        :rtype: list
        """
        return self.__incorrect_guesses

    @property
    def winner(self):
        """
        Readable winner of the game
        :return: the player who is the winner
        :rtype: Player
        """
        if len(self.__incorrect_guesses) >= 6:
            if self.__current_guesser_index == 0:
                return self.__players[1]
            elif self.__current_guesser_index == 1:
                return self.__players[0]
        elif sorted(self.__correct_guesses) == sorted(list(self.__word)) and len(self.__incorrect_guesses) < 6:
            return self.current_guesser

    @property
    def loser(self):
        """
        Readable loser of the game
        :return: the player who is the loser
        :rtype: Player
        """
        if len(self.__incorrect_guesses) >= 6:
            return self.current_guesser
        elif sorted(self.__correct_guesses) == sorted(list(self.__word)):
            if self.__current_guesser_index == 0:
                return self.__players[1]
            elif self.__current_guesser_index == 1:
                return self.__players[0]

    def play_move(self, guessed_letter):
        """
        Executes a move when the player guessing has guessed a letter
        :param guessed_letter: the letter that the player guessing has guessed
        """
        for letter in list(self.__word):
            if guessed_letter == letter:
                self.__correct_guesses.append(guessed_letter)
            elif guessed_letter not in (list(self.__word)) and guessed_letter not in self.__incorrect_guesses:
                self.__incorrect_guesses.append(guessed_letter)
        self.__letters_guessed.append(guessed_letter)
