"""The Player Class"""


class Player:
    """
    An instance of each player in the game.
    """
    def __init__(self, name):
        self.__name = name
        self.__wins = 0
        self.__losses = 0

    @property
    def name(self):
        """
        Readable player name
        :return: the Player's name
        :rtype: str
        """
        return self.__name

    @property
    def wins(self):
        """
        Readable number of wins
        :return: number of wins the player has
        :rtype: int
        """
        return self.__wins

    @property
    def losses(self):
        """
        Readable number of losses
        :return: number of losses the player has
        :rtype: int
        """
        return self.__losses

    def record_wins(self):
        """
        Adds a win to the player instance
        """
        self.__wins += 1

    def update_losses(self):
        """
        Adds a loss to the player instance
        """
        self.__losses += 1