#!/usr/bin/env python3
"""
Pex 5 Hangman Design
CS 210, Introduction to Programming
"""

__author__ = "Tricia Dang and Allison Wong"
__section__ = "M3"
__instructor__ = "Lt Col Harder"
__date__ = "1 Dec 2017"
__documentation__ = """ """
"""
Functional Requirements:
-Shall prompt the 2 players for their names.
-GUI shall begin with a full keyboard of letters to guess on the bottom, an empty gallow, and space for blanks for the word to be guessed.
-Shall prompt one player to enter a word to be guessed, then the other player to begin guessing letters.
-Shall reveal the letter in the blanks if guessed correctly, and will not allow the same letter to be guessed again.
-Shall populate the gallow with body parts as letters are guessed incorrectly.
-Shall end game if 6 letters are guessed incorrectly.
-Shall switch roles of the players at the end of each game.
-Shall provide a quit button.

Hangman Class:
This class will represent one game of Hangman.
- guess (property) - the word defined by the user to be guessed by other user. Shall be readable.
- current_guesser (property) - the player that is guessing in this game. Shall be readable.
- players (property) - a tuple of the two Player objects. Shall be readable.
- letters_guessed (property) - the letters that have been guessed so far.
- correct_guesses (property) - the letters that have been guessed correctly so far
- play_move (method) - takes the user's guessed letter, and checks to see if it is a correct guess
- winner (property) - the winner of the game, if guessed word correctly, is the guesser. if incorrect, other player
- loser (property) - the loser of the game


Player Class:
This Player class represents a single fencer. The class's constructor shall accept one parameters:
the name of the player. It shall initialize all other internal
variables that it will use to track win percentage and their record.

-name(property)- The name property shall be readable.
-wins (property) - the number of wins the player has
-losses (property) - the number of losses the player has
-record_win (method) - add a win
-record_loss (method) - add a loss
"""


def main():
    app = HangmanApp()
    app.window.mainloop()


if __name__ == '__main__':
    main()


class Hangman:
    def __init__(self, guessed_word, player1, player2):
        self.__guess = guessed_word
        self.__players = (player1, player2)
        self.__current_guesser_index = 0
        self.__letters_guessed = []
        self.__correct_guesses = []

    def play_move(self):
        pass

    def update_index(self):
        pass


class Player:
    def __init__(self, name):
        self.__name = name
        self.__wins = 0
        self.__losses = 0

    def record_wins(self):
        pass

    def update_losses(self):
        pass
