#!/usr/bin/env python3
"""
A Riveting Game of Pig, where gambling is encouraged. Two players take turns rolling a die. They either take a chance
and roll again, risking losing it all, or hold on to what they got for dear life. Practiced selection and iteration.
CS 210, Introduction to Programming
"""

import easygui
import random
import turtle

__author__ = "Allison Wong"  # Your name. Ex: John Doe
__section__ = "M3"  # Your section. Ex: M1
__instructor__ = "Lt Col Harder"  # Your instructor. Ex: Lt Col Doe
__date__ = "15 Sept 2017"  # Today's date. Ex: 25 Dec 2017
__documentation__ = """https://stackoverflow.com/questions/34033701/python-how-to-reset-the-turtle-graphics-window"""

# Define several useful constants to be used by the Turtle graphics.
WIDTH = 960  # Usually 720, 960, 1024, 1280, 1600, or 1920.
HEIGHT = WIDTH * 9 / 16  # Produces the eye-pleasing 16:9 HD aspect ratio.
MARGIN = 32  # Somewhat arbitrary value, but it looks nice.
FONT_SIZE = 16  # Somewhat arbitrary value, but it looks nice.
DRAW_FAST = True  # Set to True for fast, stealthy turtles.

DICE_SIZE = (WIDTH - (9*MARGIN))/8  # Length of one side of a dice
RADIUS = DICE_SIZE/8  # Radius of the quarter circle corner of the dice

X = (-WIDTH / 2) + MARGIN/2  # Starting x coord where first die is drawn
Y = (HEIGHT / 2) - (2 * MARGIN) - DICE_SIZE - (2 * RADIUS)  # Starting y coord where first die is drawn


def main():
    # Create the turtle screen and two turtles (leave this as the first line of main).
    screen, artist, writer = turtle_setup()
    artist.ht()
    writer.ht()

    # Set up screen
    screen.bgcolor("black")
    artist.color("white")

    # Ask user(s) for their player names
    p1_name = ask_name(1)
    p2_name = ask_name(2)
    turn = 0

    # Declare and display each player's total scores
    p1_score = 0
    p2_score = 0
    update_total_score(screen, writer, p1_score, p2_score, p1_name, p2_name)

    # Keeps the game running as long as no one's score has reached 100
    while p1_score < 100 and p2_score < 100:
        turn += 1
        if turn % 2 == 1:
            p1_score = take_one_turn(turn, p1_name, p2_name, p1_score, p2_score)
            update_total_score(screen, writer, p1_score, p2_score, p1_name, p2_name)
        elif turn % 2 == 0:
            p2_score = take_one_turn(turn, p1_name, p2_name, p1_score, p2_score)
            update_total_score(screen, writer, p1_score, p2_score, p1_name, p2_name)

    # Prints winning message when one player reaches a score of 100
    if p1_score >= 100:
        easygui.msgbox("{} wins!".format(p1_name))
    elif p2_score >= 100:
        easygui.msgbox("{} wins!".format(p2_name))

    # Wait for the user to click before closing the window (leave this as the last line of main).
    screen.exitonclick()


def ask_name(num):
    """Receives two players' name as input and returns to main()

    :param int num: number assigned to each player
    :return: str name: player names inputted by user
    """
    # Receives 2 names as input to return
    if num == 1:
        name = easygui.enterbox("Enter player 1's name.", "Player 1")
    if num == 2:
        name = easygui.enterbox("Enter player 2's name.", "Player 2")
    return name


def take_one_turn(player, p1_name, p2_name, p1_score, p2_score):
    """Player is given the option to roll or hold to their current score. If they decide to roll, a six-sided die is
    rolled and if a 1 is rolled, their turn ends and they get no points for the turn. If they decide to hold, their
    current score for the turn is added to their total score.

    :param int player: determines whose turn it is, player 1 or 2
    :param str p1_name: player 1's name
    :param str p2_name: player 2's name
    :param int p1_score: player 1's total score
    :param int p2_score: player 2's total score
    :return: int p1_score: player 1's total score returned to main()
    :return: int p2_score: player 2's total score returned to main()
    """
    dice_number = 0  # Counts how many dice have been rolled
    turn_score = 0  # The score of this turn only
    added_score = None  # The score to be added to the player's existing total score

    # Asks player 1 to roll or hold, and rolls random die. Turn ends when 1 is rolled or player decides to hold. Ar the
    # end of the turn, adds the score of the turn to the existing total score and returns the total score.
    if player%2 == 1:
        roll = easygui.boolbox("{}, would you like to roll or hold?".format(p1_name), "Choices..", ["Roll", "Hold"])
        while roll is True:
            added_score = random.randint(1, 6)
            if dice_number % 8 == 0 and dice_number != 0:
                x_start = X + (((dice_number//8) - 1) * (DICE_SIZE + MARGIN))
            else:
                x_start = X + ((dice_number % 8) * (DICE_SIZE + MARGIN))
            draw_die(added_score, x_start, Y - ((dice_number // 8) * (DICE_SIZE + MARGIN)))
            print(dice_number//8)
            if added_score == 1:
                p1_score = p1_score + 0
                roll = False
            else:
                turn_score += added_score
                dice_number += 1
                roll = easygui.boolbox("{}, would you like to roll or hold?".format(p1_name), "Choices..",
                                       ["Roll", "Hold"])
        if roll is False:
            if added_score == 1:
                return p1_score
            else:
                p1_score = p1_score + turn_score
                return p1_score

    # Asks player 2 to roll or hold, and rolls random die. Turn ends when 1 is rolled or player decides to hold. Ar the
    # end of the turn, adds the score of the turn to the existing total score and returns the total score.
    if player%2 == 0:
        roll = easygui.boolbox("{}, would you like to roll or hold?".format(p2_name), "Choices..", ["Roll", "Hold"])
        while roll is True:
            added_score = random.randint(1, 6)
            if dice_number % 8 == 0 and dice_number != 0:
                x_start = X + (((dice_number//8) - 1) * (DICE_SIZE + MARGIN))
            else:
                x_start = X + ((dice_number % 8) * (DICE_SIZE + MARGIN))
            draw_die(added_score, x_start, Y - ((dice_number // 8) * (DICE_SIZE + MARGIN)))
            if added_score == 1:
                p2_score = p2_score + 0
                roll = False
            else:
                turn_score += added_score
                dice_number += 1
                roll = easygui.boolbox("{}, would you like to roll or hold?".format(p2_name), "Choices..",
                                       ["Roll", "Hold"])
        if roll is False:
            if added_score == 1:
                return p2_score
            else:
                p2_score = p2_score + turn_score
                return p2_score


def draw_die(pips, x, y):
    """Draws the die, receiving the number as a parameter.

    :param int pips: which side of the die is rolled, a random integer 1-6
    :param float x: the x coord of bottom left starting point of the die
    :param float y: the y coord of bottom left starting point of the die
    """
    # Set artist color to white and speed to fastest
    artist = turtle.Turtle()
    artist.speed("fastest")
    artist.color("white")

    # Draw basic dice shape outline
    artist.penup()
    artist.goto(x, y)
    artist.forward(RADIUS)
    artist.pendown()
    for i in range(1, 5, 1):
        artist.forward(DICE_SIZE)
        artist.circle(RADIUS, 90)
    artist.penup()

    # Decides how many pips and draw pips on dice
    if pips % 2 == 1 and pips != 6:
        artist.goto(x + RADIUS + (DICE_SIZE/2), y + RADIUS + (DICE_SIZE/2) - (DICE_SIZE/10))
        artist.pendown()
        artist.circle(DICE_SIZE/10)
        artist.penup()
    if pips > 1:
        artist.goto(x + RADIUS + DICE_SIZE - (DICE_SIZE/10), y + RADIUS)
        artist.pendown()
        artist.circle(DICE_SIZE/10)
        artist.penup()
        artist.goto(x + RADIUS + (DICE_SIZE/10), y + RADIUS + DICE_SIZE - (DICE_SIZE/5))
        artist.pendown()
        artist.circle(DICE_SIZE/10)
        artist.penup()
    if pips > 3:
        artist.goto(x + RADIUS + DICE_SIZE - (DICE_SIZE/10), y + RADIUS + DICE_SIZE - (DICE_SIZE/5))
        artist.pendown()
        artist.circle(DICE_SIZE / 10)
        artist.penup()
        artist.goto(x + RADIUS + (DICE_SIZE/10), y + RADIUS)
        artist.pendown()
        artist.circle(DICE_SIZE / 10)
        artist.penup()
    if pips == 6:
        artist.goto(x + RADIUS + (DICE_SIZE/10), y + RADIUS + (DICE_SIZE/2) - (DICE_SIZE/10))
        artist.pendown()
        artist.circle(DICE_SIZE/10)
        artist.penup()
        artist.goto(x + RADIUS + DICE_SIZE - (DICE_SIZE/10), y + RADIUS + (DICE_SIZE/2) - (DICE_SIZE/10))
        artist.pendown()
        artist.circle(DICE_SIZE/10)
        artist.penup()

    artist.ht()


def update_total_score(screen, writer, p1_score, p2_score, p1_name, p2_name):
    """At the end of a player's turn, the player's total score is updated at the top of the screen.

    :param Turtle screen: screen, used to clear screen and allow for re-draw every turn
    :param Turtle writer: turtle used to print scores on top of the screen
    :param p1_score: player 1's total score
    :param p2_score: player 2's total score
    :param p1_name: player 1's name
    :param p2_name: player 2's name
    """
    # Clears whole screen and sets the background color to black again
    screen.clearscreen()
    screen.bgcolor("black")
    writer.color("white")

    # Writes player 1's name and score
    writer.penup()
    writer.goto((-5)*MARGIN, (HEIGHT/2) - MARGIN)
    writer.pendown()
    writer.write("{}: {}".format(p1_name, p1_score), font=("Arial", 16, "normal"))

    # Writes player 2's name and score
    writer.penup()
    writer.goto(3*MARGIN, (HEIGHT/2) - MARGIN)
    writer.pendown()
    writer.write("{}: {}".format(p2_name, p2_score), font=("Arial", 16, "normal"))

    writer.ht()


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================


def turtle_setup():
    """Setup the turtle environment with a screen and two turtles, one for drawing and one for writing.

    Using separate turtles for drawing and writing makes it easy to clear one or the other by
    doing artist.clear() or writer.clear() to clear only the drawing or writing, respectively.

    :return: The screen, a drawing turtle, and a writing turtle.
    :rtype: (turtle.Screen, turtle.Turtle, turtle.Turtle)
    """
    #  ___   ___     _  _  ___ _____    __  __  ___  ___ ___ _____   __
    # |   \ / _ \   | \| |/ _ \_   _|  |  \/  |/ _ \|   \_ _| __\ \ / /
    # | |) | (_) |  | .` | (_) || |    | |\/| | (_) | |) | || _| \ V /
    # |___/ \___/   |_|\_|\___/ |_|    |_|  |_|\___/|___/___|_|   |_|
    #  _____ _  _ ___ ___    ___ _   _ _  _  ___ _____ ___ ___  _  _
    # |_   _| || |_ _/ __|  | __| | | | \| |/ __|_   _|_ _/ _ \| \| |
    #   | | | __ || |\__ \  | _|| |_| | .` | (__  | |  | | (_) | .` |
    #   |_| |_||_|___|___/  |_|  \___/|_|\_|\___| |_| |___\___/|_|\_|
    #
    # Create the turtle graphics screen and set a few basic properties.
    screen = turtle.Screen()
    screen.setup(WIDTH, HEIGHT, MARGIN, MARGIN)
    screen.bgcolor("SkyBlue")

    # Create two turtles, one for drawing and one for writing.
    turtle.TurtleScreen._RUNNING = True  # Get around bug in v3.5.2 http://bugs.python.org/issue26571
    artist = turtle.Turtle()
    writer = turtle.Turtle()

    # Change the artist turtle's shape so the artist and writer are distinguishable.
    artist.shape("turtle")

    # Make the animation as fast as possible and hide the turtles.
    if DRAW_FAST:
        screen.delay(0)
        artist.hideturtle()
        artist.speed("fastest")
        writer.hideturtle()
        writer.speed("fastest")

    # Set a few properties of the writing turtle useful since it will only be writing.
    writer.setheading(90)  # Straight up, which makes it look sort of like a cursor.
    writer.penup()  # A turtle's pen does not have to be down to write text.
    writer.setposition(0, HEIGHT // 2 - FONT_SIZE * 2)  # Centered at top of the screen.

    return screen, artist, writer


if __name__ == "__main__":
    print(__doc__.strip())
    print("Author:", (__author__ if __author__ else "\033[91mBLANK (You must fill in the __author__ metadata)\033[0m"))
    print("Section:",
          (__section__ if __section__ else "\033[91mBLANK (You must fill in the __section__ metadata)\033[0m"))
    print("Instructor:",
          (__instructor__ if __instructor__ else "\033[91mBLANK (You must fill in the __instructor__ metadata)\033[0m"))
    print("Date:", (__date__ if __date__ else "\033[91mBLANK (You must fill in the __date__ metadata)\033[0m"))
    print("Documentation:", (
        __documentation__ if __documentation__.strip() else
        "\033[91mBLANK (You must fill in the __documentation__ metadata)\033[0m"))
    b = b'CmltcG9ydCBnZXRwYXNzLCBoYXNobGliLCBjb2RlY3MsIHN0cmluZyBhcyBfX1MKdSA9IGdldHBhc3MuZ2V0dXNlcigpCmggPSBoYXN' + \
        b'obGliLnNoYTI1Nih1LmVuY29kZSgpKS5oZXhkaWdlc3QoKQpyID0gY29kZWNzLmVuY29kZSh1Lmxvd2VyKCkudHJhbnNsYXRlKHtvcm' + \
        b'Qoayk6IE5vbmUgZm9yIGsgaW4gX19TLmRpZ2l0c30pLnJlcGxhY2UoJy4nLCcnKSwgJ3JvdF8xMycpCndpdGggb3BlbihfX2ZpbGVfX' + \
        b'ywgInIiKSBhcyBmOgogICAgaWYgaCBub3QgaW4gZi5yZWFkKCk6CiAgICAgICAgd2l0aCBvcGVuKF9fZmlsZV9fLCAiYSIpIGFzIGY6' + \
        b'CiAgICAgICAgICAgIHByaW50KCIjIiwgaCwgciwgZmlsZT1mKQo='
    try:
        import base64

        eval(compile(base64.b64decode(b), '<string>', "exec"))
    except:
        pass
    finally:
        main()
# 8fe2c883fe0bce1c11b308783baa702e2c2bb0085f6210c7daa5998870dacd19 pnyyvfbajbat
