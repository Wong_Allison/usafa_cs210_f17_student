#!/usr/bin/env python3
"""
Unit tests to use with PEX4.
"""

import sys
import unittest

__author__ = "Robert Harder"

file_to_test = "PEX4_TicTacToe.py"


class TestPEX4Functions(unittest.TestCase):
    def setUp(self):
        """
        Loads the cadet's file.  Unfortunately this re-loads for every test set that is run,
        which seems wasteful, but hey, modern OSes cache file access, right?
        """
        self.pex_module = None

        # Python 3.5
        # noinspection PyBroadException
        try:
            import importlib.util
            spec = importlib.util.spec_from_file_location("pex_module", file_to_test)
            self.pex_module = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(self.pex_module)
            from importlib.machinery import SourceFileLoader
        except:
            try:
                # Python 3.3, 3.4 (works in 3.5 but not approved solution)
                from importlib._bootstrap import SourceFileLoader
                self.pex_module = SourceFileLoader("pex_module", file_to_test).load_module()
            except Exception as e:
                print("Could not dynamically load PEX file using Python 3.4 or 3.5 technique", file=sys.stderr)
                raise e

    def tearDown(self):
        pass

    def test_player_init(self):
        """ Tests player creation """
        tests = [
            ("alice", "x", "x"),
            ("Alice", "X", "X"),
            ("bob", "o", "o"),
            ("Bob", "O", "O"),
            ("eve", "hello", "h"),
            ("eve", "", "!"),
            ("eve", None, "!")
        ]
        for name, marker_in, correct_marker in tests:
            with self.subTest("Creating player ({}, {}, {})".format(name, marker_in, correct_marker)):
                p = self.pex_module.Player(name, marker_in)  # type: Player
                self.assertEqual(name, p.name)
                self.assertEqual(correct_marker, p.marker)
                self.assertEqual(0, p.wins)
                self.assertEqual(0, p.losses)
                self.assertEqual(0, p.ties)

    def test_player_scoring(self):
        """ Tests player scoring """

        p = self.pex_module.Player("test_player_scoring", "X")  # type: Player

        with self.subTest("Testing wins"):
            self.assertEqual(0, p.wins)
            for i in range(3):
                p.record_win()
                self.assertEqual(i + 1, p.wins)

        with self.subTest("Testing losses"):
            self.assertEqual(0, p.losses)
            for i in range(3):
                p.record_loss()
                self.assertEqual(i + 1, p.losses)

        with self.subTest("Testing ties"):
            self.assertEqual(0, p.ties)
            for i in range(3):
                p.record_tie()
                self.assertEqual(i + 1, p.ties)

        with self.subTest("Testing games played"):
            self.assertEqual(9, p.games_played)

    def test_player_strings(self):
        tests = [
            ("alice", "x"),
            ("Alice", "X"),
            ("bob", "o"),
            ("Bob", "O")
        ]

        for name, marker in tests:
            with self.subTest("Test __str__ for player ({}, {})".format(name, marker)):
                p_check = self.pex_module.Player(name, marker)  # type: Player

                # Initial
                correct = "{}: {}, {} wins, {} losses, {} ties".format(name, marker, 0, 0, 0)
                self.assertEqual(correct, str(p_check))

                # One win, two losses, three ties
                p_check.record_win()
                p_check.record_loss()
                p_check.record_loss()
                p_check.record_tie()
                p_check.record_tie()
                p_check.record_tie()
                correct = "{}: {}, {} wins, {} losses, {} ties".format(name, marker, 1, 2, 3)
                self.assertEqual(correct, str(p_check))

    def test_TicTacToe(self):
        p1 = self.pex_module.Player("player1", "X")
        p2 = self.pex_module.Player("player2", "O")
        game = self.pex_module.TicTacToe(p1, p2)  # type: TicTacToe

        # Initial
        self.assertEqual(p1, game.current_player)
        self.assertEqual(0, game.turns_played)
        self.assertIsNone(game.winner)

        # Take a turn
        game.play_move(2, 2)  # center
        self.assertEqual(p2, game.current_player)
        self.assertEqual(1, game.turns_played)
        self.assertIsNone(game.winner)

        # Take a second turn
        game.play_move(1, 1)  # upper left
        self.assertEqual(p1, game.current_player)
        self.assertEqual(2, game.turns_played)
        self.assertIsNone(game.winner)

    def test_TicTacToe_winner_loser(self):
        """Tests winning and losing"""

        with self.subTest("Player 1 wins in middle row"):
            p1 = self.pex_module.Player("player1", "X")
            p2 = self.pex_module.Player("player2", "O")
            game = self.pex_module.TicTacToe(p1, p2)  # type: TicTacToe
            game.play_move(2, 2)  # 1: center            O  O
            game.play_move(1, 1)  # 2: top left          X  X  X
            game.play_move(2, 1)  # 1: left
            game.play_move(1, 2)  # 2: top
            self.assertIsNone(game.winner)
            self.assertIsNone(game.loser)
            game.play_move(2, 3)  # 1: right - winner
            self.assertEqual(p1, game.winner)
            self.assertEqual(p2, game.loser)

        with self.subTest("Player 2 wins in top row"):
            p1 = self.pex_module.Player("player1", "X")
            p2 = self.pex_module.Player("player2", "O")
            game = self.pex_module.TicTacToe(p1, p2)  # type: TicTacToe
            game.play_move(2, 2)  # 1: center            O  O  O
            game.play_move(1, 1)  # 2: top left          X  X
            game.play_move(2, 1)  # 1: left                 X
            game.play_move(1, 2)  # 2: top
            game.play_move(3, 2)  # 1: bottom
            self.assertIsNone(game.winner)
            self.assertIsNone(game.loser)
            game.play_move(1, 3)  # 2: top right
            self.assertEqual(p2, game.winner)
            self.assertEqual(p1, game.loser)

        with self.subTest("Player 1 wins in middle column"):
            p1 = self.pex_module.Player("player1", "X")
            p2 = self.pex_module.Player("player2", "O")
            game = self.pex_module.TicTacToe(p1, p2)  # type: TicTacToe
            game.play_move(2, 2)  # X: center            O  X
            game.play_move(1, 1)  # O: top left          O  X
            game.play_move(1, 2)  # X: top                  X
            game.play_move(2, 1)  # O: left
            self.assertIsNone(game.winner)
            self.assertIsNone(game.loser)
            game.play_move(3, 2)  # X: bottom - winner
            self.assertEqual(p1, game.winner)
            self.assertEqual(p2, game.loser)

        with self.subTest("Player 2 wins in left column"):
            p1 = self.pex_module.Player("player1", "X")
            p2 = self.pex_module.Player("player2", "O")
            game = self.pex_module.TicTacToe(p1, p2)  # type: TicTacToe
            game.play_move(2, 2)  # X: center            O  X  X
            game.play_move(1, 1)  # O: top left          O  X
            game.play_move(1, 2)  # X: top               O
            game.play_move(2, 1)  # O: left
            game.play_move(1, 3)  # X: top right
            self.assertIsNone(game.winner)
            self.assertIsNone(game.loser)
            game.play_move(3, 1)  # O: bottom left - winner
            self.assertEqual(p2, game.winner)
            self.assertEqual(p1, game.loser)

        with self.subTest("Player 1 wins in diagonal down and left"):
            p1 = self.pex_module.Player("player1", "X")
            p2 = self.pex_module.Player("player2", "O")
            game = self.pex_module.TicTacToe(p1, p2)  # type: TicTacToe
            game.play_move(2, 2)  # X: center            O     X
            game.play_move(1, 1)  # O: top left          O  X
            game.play_move(1, 3)  # X: top right         X
            game.play_move(2, 1)  # O: left
            self.assertIsNone(game.winner)
            self.assertIsNone(game.loser)
            game.play_move(3, 1)  # X: bottom left - winner
            self.assertEqual(p1, game.winner)
            self.assertEqual(p2, game.loser)

    def test_board_string(self):
        """ Tests the board_string function """

        with self.subTest("Brand new game"):
            p1 = self.pex_module.Player("player1", "X")
            p2 = self.pex_module.Player("player2", "O")
            game = self.pex_module.TicTacToe(p1, p2)  # type: TicTacToe
            correct = "1 2 3\n4 5 6\n7 8 9\n"
            self.assertEqual(correct, self.pex_module.board_string(game))

        with self.subTest("Player 1 wins in middle row"):
            p1 = self.pex_module.Player("player1", "X")
            p2 = self.pex_module.Player("player2", "O")
            game = self.pex_module.TicTacToe(p1, p2)  # type: TicTacToe
            game.play_move(2, 2)  # 1: center            O  O
            game.play_move(1, 1)  # 2: top left          X  X  X
            game.play_move(2, 1)  # 1: left
            game.play_move(1, 2)  # 2: top
            game.play_move(2, 3)  # 1: right - winner
            correct = "O O 3\nX X X\n7 8 9\n"
            self.assertEqual(correct, self.pex_module.board_string(game))

        with self.subTest("Player 2 wins in left column"):
            p1 = self.pex_module.Player("player1", "X")
            p2 = self.pex_module.Player("player2", "O")
            game = self.pex_module.TicTacToe(p1, p2)  # type: TicTacToe
            game.play_move(2, 2)  # X: center            O  X  X
            game.play_move(1, 1)  # O: top left          O  X
            game.play_move(1, 2)  # X: top               O
            game.play_move(2, 1)  # O: left
            game.play_move(1, 3)  # X: top right
            game.play_move(3, 1)  # O: bottom left - winner
            correct = "O X X\nO X 6\nO 8 9\n"
            self.assertEqual(correct, self.pex_module.board_string(game))


if __name__ == '__main__':
    unittest.main()
