#!/usr/bin/env python3
"""
PEX1, Fuel Gauges
CS 210, Introduction to Programming
"""

import turtle
import math

__author__ = "Allison Wong"  # Your name. Ex: John Doe
__section__ = "M3"  # Your section. Ex: M1
__instructor__ = "Lt Col Harder"  # Your instructor. Ex: Lt Col Doe
__date__ = "22 Aug 2017"  # Today's date. Ex: 25 Dec 2017
__documentation__ = """https://stackoverflow.com/questions/1823058/
                    how-to-print-number-with-commas-as-thousands-separators
                    https://stackoverflow.com/questions/16043797/python-passing-variables-between-functions
                    https://en.wikipedia.org/wiki/Circular_segment """

# Define several useful constants to be used by the Turtle graphics.
WIDTH = 960  # Usually 720, 960, 1024, 1280, 1600, or 1920.
HEIGHT = WIDTH * 9 / 16  # Produces the eye-pleasing 16:9 HD aspect ratio.
MARGIN = 32  # Somewhat arbitrary value, but it looks nice.
FONT_SIZE = 16  # Somewhat arbitrary value, but it looks nice.
DRAW_FAST = True  # Set to True for fast, stealthy turtles.
SHAPE_SIZE = (WIDTH - (MARGIN * 4)) / 3
TRIANGE_VOLUME = 0.5 * SHAPE_SIZE * SHAPE_SIZE
CIRCLE_VOLUME = math.pi * ((SHAPE_SIZE / 2) ** 2)


def main():
    # Create the turtle screen and two turtles (leave this as the first line of main).
    screen, artist, writer = turtle_setup()
    artist.ht()
    writer.ht()

    # TODO: Replace this hello world code with your code

    percentage = int(turtle.numinput("Percentage", "Please enter a percentage of fuel remaining between 50 and 95.",
                                     None, 50, 95))
    angle = None

    write_fillpercentage(percentage)
    draw_triangle_gauge()
    draw_square_gauge()
    draw_circle_gauge()
    draw_line1(percentage)
    draw_line2(percentage)
    angle = draw_line3(percentage, angle)
    write_triangle_volume(percentage)
    write_square_volume(percentage)
    write_circle_volume(percentage, angle)

    # Wait for the user to click before closing the window (leave this as the last line of main).
    screen.exitonclick()


def write_fillpercentage(percentage):
    """Prints fill percentage front and center.

    :param int percentage: the percentage of the gauge filled, input by user"""
    writer = turtle.Turtle()

    fillpercentagestartx = SHAPE_SIZE / (-2)
    fillpercentagestarty = (SHAPE_SIZE / 2) + 40
    writer.penup()
    writer.goto(fillpercentagestartx, fillpercentagestarty)
    writer.pendown()
    printed = "Fill Percentage: " + str(percentage) + "%"
    writer.write("{:^15}".format(printed), font=("Courier New", 14, "bold"))
    writer.ht()


def draw_triangle_gauge():
    """Draws the first, triangular fuel gauge"""
    artist = turtle.Turtle()

    gauge1startx = (WIDTH/(-2))+MARGIN
    gauge1starty = SHAPE_SIZE / 2
    artist.penup()
    artist.goto(artist.pos()+(gauge1startx, gauge1starty))
    artist.right(90)
    artist.pendown()
    artist.forward(SHAPE_SIZE)
    artist.left(90)
    artist.forward(SHAPE_SIZE)
    artist.left(45)
    artist.goto(gauge1startx, gauge1starty)
    artist.ht()


def draw_square_gauge():
    """Draws second, square fuel gauge"""
    artist = turtle.Turtle()

    artist.penup()
    artist.goto(-(SHAPE_SIZE / 2), SHAPE_SIZE / 2)
    artist.pendown()
    artist.forward(SHAPE_SIZE)
    artist.right(90)
    artist.forward(SHAPE_SIZE)
    artist.right(90)
    artist.forward(SHAPE_SIZE)
    artist.right(90)
    artist.forward(SHAPE_SIZE)
    artist.ht()


def draw_circle_gauge():
    """Draws third, circular fuel gauge"""
    artist = turtle.Turtle()

    gauge3startx = (SHAPE_SIZE / 2) + MARGIN + (SHAPE_SIZE / 2)
    gauge3starty = SHAPE_SIZE / (-2)
    artist.penup()
    artist.goto(gauge3startx, gauge3starty)
    artist.pendown()
    artist.circle(SHAPE_SIZE / 2)
    artist.ht()


def draw_line1(percentage):
    """Calculates and draws the red line up where the first gauge is filled

    :param int percentage: the percentage of the gauge filled, input by user"""
    artist = turtle.Turtle()

    gauge1linestartx = (WIDTH/(-2))+MARGIN
    gauge1linestarty = (SHAPE_SIZE / (-2)) + ((percentage / 100) * SHAPE_SIZE)
    gauge1starty = SHAPE_SIZE / 2
    artist.color("red")
    artist.penup()
    artist.goto(gauge1linestartx+1, gauge1linestarty)
    artist.pendown()
    artist.forward((((100-percentage)/100) * SHAPE_SIZE) - 1)
    artist.ht()


def draw_line2(percentage):
    """Calculates and draws the red line up where the second gauge is filled

    :param int percentage: the percentage of the gauge filled, input by user"""
    artist = turtle.Turtle()

    gauge2linestartx = SHAPE_SIZE / (-2)
    gauge2linestarty = SHAPE_SIZE / (-2) + ((percentage / 100) * SHAPE_SIZE)
    artist.penup()
    artist.goto(gauge2linestartx+1, gauge2linestarty)
    artist.color("red")
    artist.pendown()
    artist.forward(SHAPE_SIZE - 1)
    artist.ht()


def draw_line3(percentage, angle):
    """Calculates and draws the red line up where the third gauge is filled

    :param int percentage: the percentage of the gauge filled, input by user
    :param float angle: the angle between the red line and the radius of the circle
    :return: angle
    :rtype: float"""
    artist = turtle.Turtle()

    angle = math.asin((((percentage/100) * SHAPE_SIZE) - (SHAPE_SIZE / 2)) / (SHAPE_SIZE / 2))
    halfline = math.cos(angle)*(SHAPE_SIZE / 2)

    gauge3linestartx = (SHAPE_SIZE / 2) + MARGIN + ((SHAPE_SIZE - (2 * halfline)) / 2)
    gauge3linestarty = (SHAPE_SIZE / (-2)) + ((percentage / 100) * SHAPE_SIZE)
    artist.penup()
    artist.goto(gauge3linestartx+1, gauge3linestarty)
    artist.color("red")
    artist.pendown()
    artist.forward(2*halfline)
    artist.ht()

    return angle


def write_triangle_volume(percentage):
    """Calculates and displays the volume underneath the first red line

    :param int percentage: the percentage of the gauge filled, input by user"""
    writer = turtle.Turtle()
    volume1 = TRIANGE_VOLUME - (0.5 * ((((100 - percentage) / 100) * SHAPE_SIZE) ** 2))

    gauge1startx = (WIDTH / (-2)) + MARGIN
    gauge1starty = SHAPE_SIZE / 2
    writer.color("black")
    gauge1volumex = gauge1startx + 35
    gauge1volumey = gauge1starty - SHAPE_SIZE - 40
    writer.penup()
    writer.goto(gauge1volumex, gauge1volumey)
    writer.pendown()
    writer.write("{:^15,.2f}".format(volume1), font=("Courier New", 14, "normal"))
    writer.ht()


def write_square_volume(percentage):
    """Calculates and displays the volume underneath the second red line

    :param int percentage: the percentage of the gauge filled, input by user"""
    writer = turtle.Turtle()
    volume2 = SHAPE_SIZE * (percentage / 100) * SHAPE_SIZE

    gauge2volumex = (SHAPE_SIZE / (-2)) + 35
    gauge2volumey = (SHAPE_SIZE / (-2)) - 40
    writer.penup()
    writer.goto(gauge2volumex, gauge2volumey)
    writer.pendown()
    writer.write("{:^15,.2f}".format(volume2), font=("Courier New", 14, "normal"))
    writer.ht()


def write_circle_volume(percentage, angle):
    """Calculates and displays the volume underneath the third red line

    :param int percentage: the percentage of the gauge filled, input by user
    :param float angle: the angle between the red line and the radius of the circle"""
    writer = turtle.Turtle()
    newangle =  2*(((math.pi)/2)-angle)
    cutsegment = (((SHAPE_SIZE / 2) ** 2) / 2) * (newangle - (math.sin(newangle)))
    volume3 = CIRCLE_VOLUME - cutsegment

    gauge3volumex = (SHAPE_SIZE / 2) + MARGIN + 35
    gauge3volumey = (SHAPE_SIZE / (-2)) - 40
    writer.penup()
    writer.goto(gauge3volumex, gauge3volumey)
    writer.pendown()
    writer.write("{:^15,.2f}".format(volume3), font=("Courier New", 14, "normal"))
    writer.ht()

# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def turtle_setup():
    """Setup the turtle environment with a screen and two turtles, one for drawing and one for writing.

    Using separate turtles for drawing and writing makes it easy to clear one or the other by
    doing artist.clear() or writer.clear() to clear only the drawing or writing, respectively.

    :return: The screen, a drawing turtle, and a writing turtle.
    :rtype: (turtle.Screen, turtle.Turtle, turtle.Turtle)
    """
    # Create the turtle graphics screen and set a few basic properties.
    screen = turtle.Screen()
    screen.setup(WIDTH, HEIGHT, MARGIN, MARGIN)
    screen.bgcolor("SkyBlue")

    # Create two turtles, one for drawing and one for writing.
    turtle.TurtleScreen._RUNNING = True  # Get around bug in v3.5.2 http://bugs.python.org/issue26571
    artist = turtle.Turtle()
    writer = turtle.Turtle()

    # Change the artist turtle's shape so the artist and writer are distinguishable.
    artist.shape("turtle")

    # Make the animation as fast as possible and hide the turtles.
    if DRAW_FAST:
        screen.delay(0)
        artist.hideturtle()
        artist.speed("fastest")
        writer.hideturtle()
        writer.speed("fastest")

    # Set a few properties of the writing turtle useful since it will only be writing.
    writer.setheading(90)  # Straight up, which makes it look sort of like a cursor.
    writer.penup()  # A turtle's pen does not have to be down to write text.
    writer.setposition(0, HEIGHT // 2 - FONT_SIZE * 2)  # Centered at top of the screen.

    return screen, artist, writer


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    print(__doc__.strip())
    print("Author:", (__author__ if __author__ else "\033[91mBLANK (You must fill in the __author__ metadata)\033[0m"))
    print("Section:",
          (__section__ if __section__ else "\033[91mBLANK (You must fill in the __section__ metadata)\033[0m"))
    print("Instructor:",
          (__instructor__ if __instructor__ else "\033[91mBLANK (You must fill in the __instructor__ metadata)\033[0m"))
    print("Date:", (__date__ if __date__ else "\033[91mBLANK (You must fill in the __date__ metadata)\033[0m"))
    print("Documentation:", (
        __documentation__ if __documentation__.strip() else
        "\033[91mBLANK (You must fill in the __documentation__ metadata)\033[0m"))
    b = b'CmltcG9ydCBnZXRwYXNzLCBoYXNobGliLCBjb2RlY3MsIHN0cmluZyBhcyBfX1MKdSA9IGdldHBhc3MuZ2V0dXNlcigpCmggPSBoYXN' + \
        b'obGliLnNoYTI1Nih1LmVuY29kZSgpKS5oZXhkaWdlc3QoKQpyID0gY29kZWNzLmVuY29kZSh1Lmxvd2VyKCkudHJhbnNsYXRlKHtvcm' + \
        b'Qoayk6IE5vbmUgZm9yIGsgaW4gX19TLmRpZ2l0c30pLnJlcGxhY2UoJy4nLCcnKSwgJ3JvdF8xMycpCndpdGggb3BlbihfX2ZpbGVfX' + \
        b'ywgInIiKSBhcyBmOgogICAgaWYgaCBub3QgaW4gZi5yZWFkKCk6CiAgICAgICAgd2l0aCBvcGVuKF9fZmlsZV9fLCAiYSIpIGFzIGY6' + \
        b'CiAgICAgICAgICAgIHByaW50KCIjIiwgaCwgciwgZmlsZT1mKQo='
    try:
        import base64

        eval(compile(base64.b64decode(b), '<string>', "exec"))
    except:
        pass
    finally:
        main()
# 8fe2c883fe0bce1c11b308783baa702e2c2bb0085f6210c7daa5998870dacd19 pnyyvfbajbat
