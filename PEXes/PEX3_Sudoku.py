#!/usr/bin/env python3
"""
Game of Sudoku, in which a  9x9 grid needs to be filled, such that each row, column, and sub-3x3 grid contains only one
of each value, 1-9, inclusive.
CS 210, Introduction to Programming
"""

import easygui  # For easygui.fileopenbox.
import os  # For os.path.basename.

__author__ = "Allison Wong"  # Your name. Ex: John Doe
__section__ = "M3"  # Your section. Ex: M1
__instructor__ = "Lt Col Harder"  # Your instructor. Ex: Lt Col Doe
__date__ = "12 Oct 2017"  # Today's date. Ex: 25 Dec 2017
__documentation__ = """https://stackoverflow.com/questions/6429638/how-to-split-a-string-into-integers-in-python
                       C3C Patel informed me of the set() function"""

DATA_DIRECTORY = "../Data/Sudoku"


def main():
    """
    Main program to do Sudoku, so here's a Sudoku haiku:

    One through nine in place
    To open the matrix door
    Let logic guide you
    """

    # TODO 1b: Demonstrate the str_sudoku function with the given puzzles.
    # print(str_sudoku(PUZZLE))
    # print("\n")
    # print(str_sudoku(PUZZLE_SOLVED))

    # TODO 2b: Demonstrate the print_sudoku function with the given puzzles.
    # print_sudoku(PUZZLE)
    # print_sudoku(PUZZLE_SOLVED)

    # TODO 3b: Demonstrate the create_sudoku puzzle with the given data.
    # print_sudoku(create_sudoku(DATA))
    # print_sudoku(create_sudoku(DATA_SOLVED))

    # TODO 4b: Demonstrate the open_sudoku function with the given files.
    # for test_filename in ["Sudoku_Blank.txt", "Sudoku00.txt", "Sudoku01.txt", "Sudoku02.txt", "Sudoku03.txt"]:
    #     fullpath = os.path.join(DATA_DIRECTORY, test_filename)
    #     sudoku_data = open_sudoku(fullpath)
    #     print_sudoku(sudoku_data)

    # TODO 5b: Demonstrate the is_solved function with the given puzzles.
    # print(is_solved(PUZZLE), is_solved(PUZZLE_SOLVED))

    # TODO 6b: Demonstrate the is_valid function with the given puzzles.
    # print(is_valid(PUZZLE), is_valid(PUZZLE_SOLVED), is_valid(PUZZLE_INVALID))

    # TODO 7: Implement the main program as described
    # Allows user to select a file, passes the file path to parse data, then prints the puzzle and its info
    # filename = easygui.fileopenbox("Please select a file", "File Select")
    # while filename is not None:
    #     sudoku_data = open_sudoku(filename)
    #     print_sudoku(sudoku_data)
    #
    #     # Checks to see if the puzzle in the file is valid and/or solved
    #     if is_valid(sudoku_data) and is_solved(sudoku_data):
    #         print("The puzzle IS valid and IS solved.")
    #     elif is_valid(sudoku_data) and is_solved(sudoku_data) is False:
    #         print("The puzzle IS valid and is NOT solved.")
    #         solved = solve_sudoku(sudoku_data)  # Solves puzzle if it is valid but not solved
    #         print(solved)
    #     elif is_valid(sudoku_data) is False and is_solved(sudoku_data) is False:
    #         print("The puzzle is NOT valid and is NOT solved.")
    #     else:
    #         print("Error")
    #
    #     filename = easygui.fileopenbox("Please select a file", "File Select")

    puzzle = create_sudoku(DATA)
    print_sudoku(puzzle)
    print("Solved?", is_solved(puzzle))

    print("Solving...")
    solve_sudoku(puzzle)
    print_sudoku(puzzle)
    print("Solved?", is_solved(puzzle))


# TODO 1a: Implement the str_sudoku function as described
def str_sudoku(puzzle):
    """
    Creates a string of puzzle values suitable for printing to a file.

    The string created by this function is formatted such that it could be
    passed to create_sudoku function and recreate the same puzzle.

    Note: This function DOES NOT modify the puzzle.

    :param list[list[int]] puzzle: The Sudoku puzzle as a 9x9 nested list of integers.
    :return: A string suitable for printing to a file or passing to create_sudoku.
    :rtype: str
    """
    # In every sublist of 9 digits, the digits are joined into a string.
    for n in range(len(puzzle)):
        m = list()
        for i in range(0, 9, 1):
            m.append(" ".join(map(str, puzzle[i])))
    returned = "\n".join(map(str, m))  # The 9 strings are joined into one, big string.

    return returned


# TODO 2a: Implement the print_sudoku function as described
def print_sudoku(puzzle):
    """
    Prints the nested list structure in pretty rows and columns.

    For example, PUZZLE (bottom of this file) would print as follows:
    +===+===+===+===+===+===+===+===+===+
    # 1 | 8 |   # 6 |   | 9 #   | 5 | 7 #
    +---+---+---+---+---+---+---+---+---+
    # 5 |   |   #   |   |   #   |   | 3 #
    +---+---+---+---+---+---+---+---+---+
    #   |   | 2 #   | 8 |   # 4 |   |   #
    +===+===+===+===+===+===+===+===+===+
    # 7 |   |   # 8 | 4 | 5 #   |   | 1 #
    +---+---+---+---+---+---+---+---+---+
    #   |   | 3 # 2 |   | 1 # 9 |   |   #
    +---+---+---+---+---+---+---+---+---+
    # 2 |   |   # 9 | 6 | 3 #   |   | 5 #
    +===+===+===+===+===+===+===+===+===+
    #   |   | 1 #   | 5 |   # 8 |   |   #
    +---+---+---+---+---+---+---+---+---+
    # 8 |   |   #   |   |   #   |   | 4 #
    +---+---+---+---+---+---+---+---+---+
    # 9 | 4 |   # 3 |   | 8 #   | 7 | 2 #
    +===+===+===+===+===+===+===+===+===+

    Note: This function MUST NOT modify the puzzle.

    :param list[list[int]] puzzle: The Sudoku puzzle as a 9x9 nested list of integers.
    :return: None
    """
    for i in range(0, 19, 1):
        # Checks if the row is an outline, filler line, or a number-containing line
        if i in (0, 6, 12, 18):
            print("+===+===+===+===+===+===+===+===+===+")
        elif i in (2, 4, 8, 10, 14, 16):
            print("+---+---+---+---+---+---+---+---+---+")
        else:
            for j in range(0, 37, 1):
                # Checks across the number-containing row to print a hash, line, space, or number
                if j in (0, 12, 24, 36):
                    print("#", end="")
                    if j == 36:
                        print("\n", end="")  # Checks if it is the last column of the row and returns to a new line
                elif j in (4, 8, 16, 20, 28, 32):
                    print("|", end="")
                elif (j + 1) % 2 == 0:
                    print(" ", end=""),
                else:  # A number needs to be printed. Checks which number in the matrix to print
                    # If it is a 0, replace with a space
                    if puzzle[int((i - 1) / 2)][int((j - 2) / 4)] == 0:
                        print(" ", end="")
                    else:
                        print(puzzle[int((i - 1) / 2)][int((j - 2) / 4)], end="")


# TODO 3a: Implement the create_sudoku function as described
def create_sudoku(data):
    """
    Creates a 9x9 nested list of integers from a string with 81 separate values.

    :param str data: A string with 81 separate integer values, [0-9].
    :return: The Sudoku puzzle as a 9x9 nested list of integers.
    :rtype: list[list[int]]
    """
    all_81 = [int(n) for n in data.split()]  # Turns a string of numbers into a list of numbers

    i = 0
    returned = list()
    while i < len(all_81):
        # Turns into every 9 digits of the list into a sublist that is added to into the larger list
        returned.append(all_81[i:i + 9])
        i += 9  # Will go to the next 9 digits next time

    return returned


# TODO 4a: Implement the open_sudoku function as described
def open_sudoku(filename):
    """
    Opens the given file, parses the contents, and returns a Sudoku puzzle.

    This function prints to the console any comment lines in the file.

    :param str filename: The file name.
    :return: The Sudoku puzzle as a 9x9 nested list of integers.
    :rtype: list[list[int]]
    """
    # Opens the file containing the sudoku data, potentially with comments, and splits into list of lines
    with open(filename) as data_file:
        print(os.path.basename(filename))
        data_lines = data_file.read().splitlines()

    i = 0
    nums_only = ""  # Will contain all 81 digits as a string
    while i < len(data_lines):
        data_char = data_lines[i].split()
        if data_char[0] == "#":  # If line begins with a hash, the line, excluding the hash, is printed in console
            print((data_lines[i])[1:])
        try:
            int(data_char[0])
            nums_only += data_lines[i] + " "  # Adds the numbers in that line to nums_only
        except ValueError:
            pass
        i += 1

    returned = create_sudoku(nums_only)  # Turns string of digits from the file into nested lists
    return returned


# TODO 5a: Implement the is_solved function as described
def is_solved(puzzle):
    """
    Determines if a Sudoku puzzle is valid and complete.

    Note: This function MUST NOT modify the puzzle.

    :param list[list[int]] puzzle: The Sudoku puzzle as a 9x9 nested list of integers.
    :return: True if the puzzle is valid and complete; False otherwise.
    :rtype: bool
    """
    # If any of the numbers are 0, aka blank, the puzzle is not solved, and returns False
    for i in range(len(puzzle[0])):
        for j in range(len(puzzle[0])):
            if puzzle[i][j] == 0:
                return False
    if not is_valid(puzzle):
        return False

    return True


# TODO 6a: Implement the is_valid function as described
def is_valid(puzzle):
    """
    Determines if a Sudoku puzzle is valid, but not necessarily complete.

    Note: This function MUST NOT modify the puzzle.

    :param list[list[int]] puzzle: The Sudoku puzzle as a 9x9 nested list of integers.
    :return: True if the puzzle is valid; False otherwise.
    :rtype: bool
    """
    for i in range(len(puzzle[0])):  # Tests all the rows of numbers if it is a valid set of 9
        for a in range(1, 9, 1):
            if puzzle[i].count(a) > 1:  # Returns False if the count of any number 1-9 is greater than 1
                return False

    for j in range(len(puzzle[0])):  # Tests all the columns of numbers if it is a valid set of 9
        n = list()
        for k in range(len(puzzle[0])):
            # If the number is zero, it is not added to the list that will be examined for repetition
            if puzzle[k][j] != 0:
                n.append(puzzle[k][j])
        if len(n) != len(set(n)):  # Returns False if there are any repeated digits
            return False
        del n  # Clears list to be used again for the next column

    for l in range(0, len(puzzle[0]), 3):  # Tests all the 3x3 grids of numbers if it is a valid set of 9
        for s in range(0, len(puzzle[0]), 3):
            r = list()
            # Loops through a 3 by 3 grid of the matrix and adds values to a list
            for m in range(l, l+3, 1):
                for p in range(s, s+3, 1):
                    if puzzle[m][p] != 0:
                        r.append(puzzle[m][p])
            if len(r) != len(set(r)):
                return False  # Returns False if there are any repeated digits
            del r
            if s == 8:
                s = 0
        if l == 8:
            l = 0

    return True


# TODO 8: Implement the solve_sudoku function as discussed in class
def solve_sudoku(puzzle, cell=0, value=1):
    """
    Recursive function to solve a Sudoku puzzle with brute force.

    Note: This function DOES modify the puzzle!!!

    :param list[list[int]] puzzle:  The 9x9 nested list of integers.
    :return: None
    """
    r = cell // 9
    c = cell % 9

    if is_solved(puzzle) or not is_valid(puzzle) or cell > 80 or value > 9:
        return
    elif puzzle[r][c] != 0:
        solve_sudoku(puzzle, cell + 1)
    else:
        puzzle[r][c] = value
        solve_sudoku(puzzle, cell + 1)

        if is_solved(puzzle):
            pass
        else:
            puzzle[r][c] = 0
            solve_sudoku(puzzle, cell, value + 1)

"""
The following puzzles in list and string form are provided to help you
test and demonstrate your code.
"""

PUZZLE = [[1, 8, 0, 6, 0, 9, 0, 5, 7],
          [5, 0, 0, 0, 0, 0, 0, 0, 3],
          [0, 0, 2, 0, 8, 0, 4, 0, 0],
          [7, 0, 0, 8, 4, 5, 0, 0, 1],
          [0, 0, 3, 2, 0, 1, 9, 0, 0],
          [2, 0, 0, 9, 6, 3, 0, 0, 5],
          [0, 0, 1, 0, 5, 0, 8, 0, 0],
          [8, 0, 0, 0, 0, 0, 0, 0, 4],
          [9, 4, 0, 3, 0, 8, 0, 7, 2]]

PUZZLE_SOLVED = [[1, 8, 4, 6, 3, 9, 2, 5, 7],
                 [5, 9, 7, 1, 2, 4, 6, 8, 3],
                 [6, 3, 2, 5, 8, 7, 4, 1, 9],
                 [7, 6, 9, 8, 4, 5, 3, 2, 1],
                 [4, 5, 3, 2, 7, 1, 9, 6, 8],
                 [2, 1, 8, 9, 6, 3, 7, 4, 5],
                 [3, 7, 1, 4, 5, 2, 8, 9, 6],
                 [8, 2, 5, 7, 9, 6, 1, 3, 4],
                 [9, 4, 6, 3, 1, 8, 5, 7, 2]]

PUZZLE_INVALID = [[1, 8, 0, 6, 3, 9, 0, 5, 7],
                  [5, 0, 0, 0, 0, 0, 0, 0, 3],
                  [0, 0, 2, 0, 8, 0, 4, 0, 0],
                  [7, 0, 0, 8, 4, 5, 0, 0, 1],
                  [0, 0, 3, 2, 3, 1, 9, 0, 0],
                  [2, 0, 0, 9, 6, 3, 0, 0, 5],
                  [0, 0, 1, 0, 5, 0, 8, 0, 0],
                  [8, 0, 0, 0, 0, 0, 0, 0, 4],
                  [9, 4, 0, 3, 0, 8, 0, 7, 2]]

DATA = """1 8 0 6 0 9 0 5 7
          5 0 0 0 0 0 0 0 3
          0 0 2 0 8 0 4 0 0
          7 0 0 8 4 5 0 0 1
          0 0 3 2 0 1 9 0 0
          2 0 0 9 6 3 0 0 5
          0 0 1 0 5 0 8 0 0
          8 0 0 0 0 0 0 0 4
          9 4 0 3 0 8 0 7 2"""

DATA_SOLVED = """1 8 4 6 3 9 2 5 7
                 5 9 7 1 2 4 6 8 3
                 6 3 2 5 8 7 4 1 9
                 7 6 9 8 4 5 3 2 1
                 4 5 3 2 7 1 9 6 8
                 2 1 8 9 6 3 7 4 5
                 3 7 1 4 5 2 8 9 6
                 8 2 5 7 9 6 1 3 4
                 9 4 6 3 1 8 5 7 2"""

# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    print(__doc__.strip())
    print("Author:", (__author__ if __author__ else "\033[91mBLANK (You must fill in the __author__ metadata)\033[0m"))
    print("Section:",
          (__section__ if __section__ else "\033[91mBLANK (You must fill in the __section__ metadata)\033[0m"))
    print("Instructor:",
          (__instructor__ if __instructor__ else "\033[91mBLANK (You must fill in the __instructor__ metadata)\033[0m"))
    print("Date:", (__date__ if __date__ else "\033[91mBLANK (You must fill in the __date__ metadata)\033[0m"))
    print("Documentation:", (
        __documentation__ if __documentation__.strip() else
        "\033[91mBLANK (You must fill in the __documentation__ metadata)\033[0m"))
    _ = b'CmltcG9ydCBnZXRwYXNzLCBoYXNobGliLCBjb2RlY3MsIHN0cmluZyBhcyBfX1MKdSA9IGdldHBhc3MuZ2V0dXNlcigpCmggPSBoYXN' + \
        b'obGliLnNoYTI1Nih1LmVuY29kZSgpKS5oZXhkaWdlc3QoKQpyID0gY29kZWNzLmVuY29kZSh1Lmxvd2VyKCkudHJhbnNsYXRlKHtvcm' + \
        b'Qoayk6IE5vbmUgZm9yIGsgaW4gX19TLmRpZ2l0c30pLnJlcGxhY2UoJy4nLCcnKSwgJ3JvdF8xMycpCndpdGggb3BlbihfX2ZpbGVfX' + \
        b'ywgInIiKSBhcyBmOgogICAgaWYgaCBub3QgaW4gZi5yZWFkKCk6CiAgICAgICAgd2l0aCBvcGVuKF9fZmlsZV9fLCAiYSIpIGFzIGY6' + \
        b'CiAgICAgICAgICAgIHByaW50KCIjIiwgaCwgciwgZmlsZT1mKQo='
    # noinspection PyBroadException
    try:
        import base64

        eval(compile(base64.b64decode(_), '<string>', "exec"))
    except:
        pass
    finally:
        main()
# 8fe2c883fe0bce1c11b308783baa702e2c2bb0085f6210c7daa5998870dacd19 pnyyvfbajbat
